## classes

| id | qr_code_id | creator_id | student_group_id | begin_date          | end_date            | name_ue             |
|----|------------|------------|------------------|---------------------|---------------------|---------------------|
|  1 |          1 |         26 |                1 | 2019-10-24 08:00:00 | 2019-10-24 10:00:00 | M1INF01 - PHP       |


## fos_user

| id | function_id | username   | username_canonical | email               | email_canonical     | enabled | salt | password                                                     | last_login          | confirmation_token | password_requested_at | roles                              | informationStudent | informationCompany | informationTeacher | informationAdministrator |
|----|-------------|------------|--------------------|---------------------|---------------------|---------|------|--------------------------------------------------------------|---------------------|--------------------|-----------------------|------------------------------------|--------------------|--------------------|--------------------|--------------------------|
|  1 |           1 | company    | company            | contact@tecknet.com | contact@tecknet.com |       1 | NULL | <hash> | NULL                | NULL               | NULL                  | a:0:{}                             |               NULL |                  1 |               NULL |                     NULL |


## information_administrator

| id |
|----|
|  1 |

## information_company

| id | name         |
|----|--------------|
|  1 | Tecknet Inc. |

## information_student

| id | company_id | student_group_id | start_date_contrat  | end_date_contrat    |
|----|------------|------------------|---------------------|---------------------|
|  1 |          1 |                1 | 2019-09-09 08:00:00 | 2020-07-31 18:00:00 |


## information_teacher

| id |
|----|
|  1 |

## qr_code

| id | value  |
|----|--------|
|  1 | 111111 |

## role

| id | rolename      |
|----|---------------|
|  1 | Company       |


## student_group

| id | creator_teacher_id | group_name |
|----|--------------------|------------|
|  1 |                  1 | Group A    |

## validation_presence