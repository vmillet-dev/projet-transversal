## To deploy this web server:
 * Install symfony required libraries

```bash
apt install composer php7.2 php7.2-xml php7.2-mysql libicu-dev libmcrypt-dev libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libldap2-dev unixodbc-dev libpq-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev -yqq
```

 * Have a viable mysql DB configuration and edit `config/db_parameters.yml`
 * Install the vendor components:

 ```bash
php composer.phar install
```

 * Create database && schema

```bash
php bin/console doctrine:database:create --if-not-exists
php bin/console doctrine:schema:create
```

 * Load fixtures (*Optionnal)
```bash
php bin/console doctrine:fixtures:load -n --env=test
```

 * Test (*Optionnal, need fixtures)

```bash
curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit
phpunit -c phpunit.xml.dist
```

 * Syntax validation (*Optionnal)

```bash
curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar
php phpcs.phar --standard=PSR1,PSR12 --extensions=php,module,inc,install,test,profile,theme,scss,yml config public src templates translations
```

 * Start the server

```bash
php bin/console server:start
```