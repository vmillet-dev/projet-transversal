# Projet transversal - M1IF10

Notre projet se base autour d'une problématique récurrente chez un certain nombre d'étudiants et d'étudiantes en
alternance. En effet, leur obligation de présence implique l'émargement à chaque cours pour valider un nombre d'heures
, cela peut vite devenir fastidieux avec parfois plus d'une centaine d'étudiants.

L'utilisation d'outils numériques permettra d'alléger la procédure, que cela soit du côté de l'étudiant,
du prof ou de l'entreprise. 

## Installation 

Les instructions pour lancer le serveur Symfony se trouve dans le fichier [INSTALLME.md](INSTALLME.md).

## Pour commencer

Une fois que cela est fait (ou via la [VM](https://192.168.74.209)), nous avons un site web contenant un accueil 
pour s'enregistrer ou se connecter. Si les fixtures ont été chargés, plusieurs utilisateurs sont définis de facto.


| Pseudo | Mdp | Role |
| ------ | ------ | ------ |
| teacher | teacher | Professeur |
| company | company | Entreprise |
| student | student | Alternant |
| admin | admin | Administrateur |

On peut les utiliser pour tester les différentes possibilités d'utilisation en fonction du rôle. 

## Structure du projet

Symfony utilise une approche très modulaire du modèle de conception MVC.

* Les modèles sont nommés Entités (en raison des données relationnelles qui y sont ajoutées pour l'ORM).
* Les noms des contrôleurs (utilisés pour lier le côté serveur) et les paths (utilisés pour les liens entrants 
ou dans les scripts) peuvent être définis par des commentaires avant leur déclaration.
* Les vues sont appelées templates. Ils peuvent étendre (hériter et modifier des "blocs") 
ou inclure (intégrer totalement) d'autres templates (d'où le nom de "template").

|name      |Directory       |
|:-------- |:-------------- |
|Modele    |src/Entity/     |
|Vue      |templates/      |
|Contrôleur|src/Controller/ |
|Fixtures   | src/DataFixtures |
|Test   | tests/    |

## Structure des données

Nous avons ici l'ensemble des entités utilisées par la partie modele. Cela permet d'avoir une vue globale de toutes
les tables 

|       Entités             |  Description |
|:-------------------------- | --------------------------    |
| classes                    | Cours donnés par un prof à des élèves |
| fos_user                   |  |
| information_administrator  | |
| information_company        | |
| information_student        | |
| information_teacher        | |
| qr_code                    | valeur d'un QR Code associé à un cours |
| role                       | role de l'utilisateur |
| student_group              | groupe d'un étudiant |
| validation_presence        | validation de la présence d'un étudiant à un cours |

Vous avez à votre disposition [ici](FIXTURES.md) des exemple d'entités avec un contenu possible.

## Tests

Nous avons effectué une converture à plus de 75 % de notre application via des tests unitaires. Nous couvrons
principalement les controleurs qui, eux, couvrent l'ensemble de l'application et du modèle. Ils se trouvent
dans le dossier [test/](tests).

## VM

L'IP de notre VM est [192.168.74.209](https://192.168.74.209), verifiez bien que la connexion soit en HTTPS
(acceptez les risques lorsque cela est demandé sinon l'utilisation de la caméra est impossible)

