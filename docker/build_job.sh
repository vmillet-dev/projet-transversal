# Hash the dockerfile
DOCKER_TAG=`md5sum ./Dockerfile | awk '{ print $1 }'`

# Pull the image
docker pull forge.univ-lyon1.fr:4567/p1709476/projet_transversal_g11:$DOCKER_TAG

# (image-tag == Dockerfile hash) => no image update => no rebuild needed
if [ $? -eq 0  ]; then
    exit;
fi

docker build . -t forge.univ-lyon1.fr:4567/p1709476/projet_transversal_g11:$DOCKER_TAG
docker push forge.univ-lyon1.fr:4567/p1709476/projet_transversal_g11:$DOCKER_TAG
