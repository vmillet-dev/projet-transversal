function CreateCheckboxForClassList(idStudent) {
    let checkbox = document.createElement("input");
    checkbox.setAttribute("name", "StudentList[]");
    checkbox.setAttribute("type", "checkbox");
    checkbox.setAttribute("id", "check" + idStudent);
    checkbox.setAttribute("value", idStudent);
    checkbox.style.display = "none";
    checkbox.checked = true;


    let form = document.getElementById("formClassList");
    form.appendChild(checkbox);
}

function DeleteCheckboxForClassList(idStudent) {
    let checkbox = document.getElementById("check" + idStudent);
    checkbox.parentNode.removeChild(checkbox);
}

function TransferToClassList(element) {
    let bodyClassList = document.getElementById("bodyClassList");
    let row = element.parentNode.parentNode;
    let span = element.childNodes[1];

    span.removeAttribute("class");
    span.setAttribute("class", "glyphicon glyphicon-circle-arrow-left");

    element.removeAttribute("onclick");
    element.setAttribute("onclick", "TransferToStudentList(this)");

    bodyClassList.appendChild(row);

    let idStudent = row.childNodes[5].childNodes[1].value;
    CreateCheckboxForClassList(idStudent);
}

function TransferToStudentList(element) {
    let bodyStudentList = document.getElementById("bodyStudentList");
    let row = element.parentNode.parentNode;
    let span = element.childNodes[1];

    span.removeAttribute("class");
    span.setAttribute("class", "glyphicon glyphicon-circle-arrow-right");

    element.removeAttribute("onclick");
    element.setAttribute("onclick", "TransferToClassList(this)");

    bodyStudentList.appendChild(row);

    let idStudent = row.childNodes[5].childNodes[1].value;
    DeleteCheckboxForClassList(idStudent);
}