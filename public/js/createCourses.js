function CreateCheckboxForClassList(idClasses) {
    let checkbox = document.createElement("input");
    checkbox.setAttribute("name", "ClasseSelected");
    checkbox.setAttribute("type", "checkbox");
    checkbox.setAttribute("id", "check" + idClasses);
    checkbox.setAttribute("value", idClasses);
    checkbox.style.display = "none";
    checkbox.checked = true;


    let form = document.getElementById("formClassList");
    form.appendChild(checkbox);
}

function DeleteCheckboxForClassList(idStudent) {
    let checkbox = document.getElementById("check" + idStudent);
    checkbox.parentNode.removeChild(checkbox);
}

function CleanCoursesList() {
    let bodyCoursesList = document.getElementById("bodyCoursesList");
    let nbElement = bodyCoursesList.childElementCount;
    for (let i = 0; i < nbElement; i++) {
        let firstRow = bodyCoursesList.childNodes[0]
        let button = firstRow.getElementsByTagName("button")[0];
        TransferToClassList(button);
    }
}

function TransferToCoursesList(element) {
    CleanCoursesList();
    let bodyCoursesList = document.getElementById("bodyCoursesList");
    let row = element.parentNode.parentNode;
    let span = element.childNodes[1];

    span.removeAttribute("class");
    span.setAttribute("class", "glyphicon glyphicon-circle-arrow-left");

    element.removeAttribute("onclick");
    element.setAttribute("onclick", "TransferToClassList(this)");

    bodyCoursesList.appendChild(row);

    let idClasses = row.childNodes[5].childNodes[1].value;
    CreateCheckboxForClassList(idClasses);
}

function TransferToClassList(element) {;
    let bodyClassList = document.getElementById("bodyClassList");
    let row = element.parentNode.parentNode;
    let span = element.childNodes[1];

    span.removeAttribute("class");
    span.setAttribute("class", "glyphicon glyphicon-circle-arrow-right");

    element.removeAttribute("onclick");
    element.setAttribute("onclick", "TransferToCoursesList(this)");

    bodyClassList.appendChild(row);

    let idClasses = row.childNodes[5].childNodes[1].value;
    DeleteCheckboxForClassList(idClasses);
}