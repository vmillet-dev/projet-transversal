function openDrawerMenu() {
    var x = document.getElementById("mainNavBar");
    if (x.className === "navBar") {
        x.className += " responsive";
    } else {
        x.className = "navBar";
    }
}

function redirectToModifyUser(Id) {
    document.location.href = "/modifyUser/" + Id;
}

function redirectToStudentCourses(Id) {
    document.location.href = "/studentCourses/" + Id;
}