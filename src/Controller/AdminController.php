<?php

namespace App\Controller;

use App\Entity\InformationAdministrator;
use App\Entity\InformationStudent;
use App\Entity\User;
use App\Entity\Role;
use App\Entity\InformationCompany;
use App\Entity\InformationTeacher;
use App\Form\EditAdministratorInformation;
use App\Form\EditStudentInformation;
use App\Form\EditCompanyInformation;
use App\Form\EditTeacherInformation;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class AdminController extends AbstractController
{
    public function isNotAdministrator(): bool
    {
        $function = $this->container->get('security.token_storage')->getToken()
            ->getUser()->getFunction();
        if ($function == null) {
            return true;
        }
        $rolename = $function->getRoleName();
        return $rolename != "Administrator";
    }

    public function error403()
    {
        //throw new AccessDeniedException('Only teacher can see this page');
        $resp = new Response();
        $resp->setStatusCode(Response::HTTP_FORBIDDEN);
        $resp->send();
        return $resp;
    }

    public function error404()
    {
        //throw new NotFoundHttpException("Page Not Found");
        $resp = new Response();
        $resp->setStatusCode(Response::HTTP_NOT_FOUND);
        $resp->send();
        return $resp;
    }

    /**
     * @Route("/adminDashboard", name="adminDashboard")
     */
    public function adminDashboard()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isNotAdministrator()) {
            return $this->error403();
        }

        $repository = $this->getDoctrine()->getRepository(User::class);

        $view_param = [
            'userList' => $repository->getAllUser(),
        ];
        return $this->render('admin/adminDashboard/dashboard.html.twig', $view_param);
    }

    public function createFormModifyUser($formUser)
    {
        return $this->createFormBuilder($formUser)
            ->add(
                'function',
                EntityType::class,
                [
                    'class' => Role::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                            ->orderBy('c.rolename', 'ASC');
                    },
                    'choice_label' => 'rolename',
                ]
            )
            ->add('informationStudent', EditStudentInformation::class)
            ->add('informationCompany', EditCompanyInformation::class)
            ->add('informationTeacher', EditTeacherInformation::class)
            ->add('informationAdministrator', EditAdministratorInformation::class)
            ->add('save', SubmitType::class)
            ->getForm();
    }

    public function setNullOldRole($newInformation)
    {
        $role = $newInformation->getFunction()->getRoleName();
        if ($role != "Student") {
            $newInformation->setInformationStudent(null);
        }
        if ($role != "Company") {
            $newInformation->setInformationCompany(null);
        }
        if ($role != "Administrator") {
            $newInformation->setInformationAdministrator(null);
        }
        if ($role != "Teacher") {
            $newInformation->setInformationTeacher(null);
        }

        return $newInformation;
    }

    /**
     * @Route("/modifyUser/{idUser}", name="modifyUser")
     */
    public function modifyUser(Request $request, int $idUser)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isNotAdministrator()) {
            return $this->error403();
        }

        $repository = $this->getDoctrine()->getRepository(User::class);
        $user = $repository->find($idUser); //Is null when this $idUser isn't in the database
        if ($user != null) {
            $form = $this->createFormModifyUser($user);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $newInformation = $form->getData();
                $repository = $this->getDoctrine()->getRepository(User::class);

                $newInformation = $this->setNullOldRole($newInformation);
                $repository->updateInformationUser($newInformation);
            }

            $repository = $this->getDoctrine()->getRepository(Role::class);
            $idStudent = $repository->getIdRole("student");
            $idCompany = $repository->getIdRole("company");
            $idTeacher = $repository->getIdRole("teacher");
            $idAdministrator = $repository->getIdRole("administrator");
            $view_param = [
                'form' => $form->createView(),
                'user' => $user,
                'idStudent' => $idStudent,
                'idCompany' => $idCompany,
                'idTeacher' => $idTeacher,
                'idAdministrator' => $idAdministrator
            ];
            return $this->render('admin/modifyUser/modifyUser.html.twig', $view_param);
        } else {
            return $this->error404();
        }
    }
}
