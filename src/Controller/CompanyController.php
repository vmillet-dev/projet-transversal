<?php

namespace App\Controller;

use App\Entity\InformationStudent;
use App\Entity\Classes;
use App\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;

class CompanyController extends AbstractController
{
    public function isNotCompany(): bool
    {
        $function = $this->container->get('security.token_storage')->getToken()
            ->getUser()->getFunction();
        if ($function == null) {
            return true;
        }
        $rolename = $function->getRoleName();
        return $rolename != "Company";
    }

    public function error403()
    {
        //throw new AccessDeniedException('Only teacher can see this page');
        $resp = new Response();
        $resp->setStatusCode(Response::HTTP_FORBIDDEN);
        $resp->send();
        return $resp;
    }

    /**
     * @Route("/studentList", name="studentList")
     */
    public function studentList()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isNotCompany()) {
            return $this->error403();
        }

        $repository = $this->getDoctrine()->getRepository(User::class);
        $idCompany = $this->container->get('security.token_storage')
            ->getToken()
            ->getUser()
            ->getInformationCompany()
            ->getId();
        $userList = $repository->getStudentByCompany($idCompany);
        $view_param = [
            'userList' => $userList,
        ];
        return $this->render('company/studentList/studentList.html.twig', $view_param);
    }

    public function studentIsInCompany($idStudent)
    {
        $informationCompany = $this->container
            ->get('security.token_storage')
            ->getToken()
            ->getUser()
            ->getInformationCompany();
        $repository = $this->getDoctrine()->getRepository(InformationStudent::class);
        $informationStudent = $repository->getInformationStudentById($idStudent);

        if ($informationStudent == null || $informationStudent->getCompany() != $informationCompany) {
            return false;
        }
        return true;
    }

    /**
     * @Route("/studentCourses/{idStudent}", name="studentCourses")
     */
    public function studentCourses($idStudent)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isNotCompany() || !$this->studentIsInCompany($idStudent)) {
            return $this->error403();
        }

        $repository = $this->getDoctrine()->getRepository(Classes::class);
        $listPresent = $repository->getClassesWhereStudentIsPresent($idStudent);
        foreach ($listPresent as $courses) {
            $courses->setStudentPresence("Present");
        }

        $listAbsent = $repository->getClassesWhereStudentIsAbsent($idStudent);
        foreach ($listAbsent as $courses) {
            $courses->setStudentPresence("Absent");
        }

        $listFutur = $repository->getClassesByStudentInFutur($idStudent);
        foreach ($listFutur as $courses) {
            $courses->setStudentPresence("Futur");
        }

        $coursesList = array_merge($listPresent, $listAbsent, $listFutur);
        usort($coursesList, function ($a, $b) {
            return $a->getBeginDate() > $b->getBeginDate();
        });

        $view_param = [
            'coursesList' => $coursesList,
        ];
        return $this->render('company/courses/courses.html.twig', $view_param);
    }
}
