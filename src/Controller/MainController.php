<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\StudentGroup;
use App\Entity\InformationStudent;
use App\Entity\Classes;
use App\Entity\QrCode;
use App\Entity\User;
use App\Entity\Courses;
use App\Entity\ValidationPresence;
use App\Helper\HelperIdCurrentUser;
use DateTime;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home()
    {
        $user = $this->container->get('security.token_storage')->getToken()
            ->getUser();
        if (is_string($user)) {
            return $this->render('home/homeAdmin.html.twig');
        }
        $function = $user->getFunction();
        if ($function == null) {
            return $this->render('home/homeNoInformation.html.twig');
        }
        $roleName = $function->getRoleName();
        switch ($roleName) {
            case "Student":
                return $this->render('home/homeStudent.html.twig');
            case "Company":
                return $this->render('home/homeCompany.html.twig');
            case "Teacher":
                return $this->render('home/homeTeacher.html.twig');
            case "Administrator":
                return $this->render('home/homeAdmin.html.twig');
            default:
                return $this->render('home/homeNoInformation.html.twig');
        }
    }

    // public function createQrCode(): QrCode
    // {
    //     $qrCodeValue = random_int(0, 1000000);
    //     $repository = $this->getDoctrine()->getRepository(QrCode::class);
    //     $qrCode = $repository->addQrCode($qrCodeValue);
    //     return $qrCode;
    // }
}
