<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\StudentGroup;
use App\Entity\InformationStudent;
use App\Entity\Classes;
use App\Entity\QrCode;
use App\Entity\User;
use App\Entity\Courses;
use App\Entity\ValidationPresence;
use App\Helper\HelperIdCurrentUser;
use DateTime;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class StudentController extends AbstractController
{
    public function isNotStudent(): bool
    {
        $function = $this->container->get('security.token_storage')->getToken()
            ->getUser()->getFunction();
        if ($function == null) {
            return true;
        }
        $rolename = $function->getRoleName();
        return $rolename != "Student";
    }

    public function error403()
    {
        //throw new AccessDeniedException('Only teacher can see this page');
        $resp = new Response();
        $resp->setStatusCode(Response::HTTP_FORBIDDEN);
        $resp->send();
        return $resp;
    }

    public function error404()
    {
        //throw new NotFoundHttpException("Page Not Found");
        $resp = new Response();
        $resp->setStatusCode(Response::HTTP_NOT_FOUND);
        $resp->send();
        return $resp;
    }

    /**
     * @Route("/scanCode", name="ScanCode")
     */
    public function scanCode()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isNotStudent()) {
            return $this->error403();
        }
        return $this->render('QrCode/ScanQRCode.html.twig');
    }

    /**
     * @Route("/verifCode/{resultCode}", name="VerifCode")
     */
    public function verifCode(int $resultCode)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $idUtilisateur = HelperIdCurrentUser::getIdUserCurrent($this->container);

        if (!$resultCode) {
            return $this->error404();
        }

        if ($this->isNotStudent()) {
            return $this->error403();
        }

        $idStudent = $this->container->get('security.token_storage')->getToken()
            ->getUser()->getInformationStudent()->getId();

        $classRepository = $this->getDoctrine()->getRepository(Classes::class);
        $result = $classRepository->getClassesByStudentAndCode($idStudent, $resultCode);
        if (empty($result)) {
            return $this->error404();
        }
        $cour = $result[0];

        $presenceRepository = $this->getDoctrine()->getRepository(ValidationPresence::class);

        $user = $this->getUser()->getInformationStudent();
        $presenceRepository->addPresence($user, $cour);

        $resp = new Response();
        $resp->setStatusCode(Response::HTTP_OK);
        $resp->send();
        return $resp;
    }

    /**
     * @Route("/courses", name="courses")
     */
    public function courses()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isNotStudent()) {
            return $this->error403();
        }

        $idUtilisateur = HelperIdCurrentUser::getIdUserCurrent($this->container);
        $repository = $this->getDoctrine()->getRepository(User::class);
        $user = $repository->find($idUtilisateur);

        $repository = $this->getDoctrine()->getRepository(Classes::class);
        $idStudent = $user->getInformationStudent()->getId();
        $listPresent = $repository->getClassesWhereStudentIsPresent($idStudent);
        foreach ($listPresent as $courses) {
            $courses->setStudentPresence("Present");
        }

        $listAbsent = $repository->getClassesWhereStudentIsAbsent($idStudent);
        foreach ($listAbsent as $courses) {
            $courses->setStudentPresence("Absent");
        }

        $listFutur = $repository->getClassesByStudentInFutur($idStudent);
        foreach ($listFutur as $courses) {
            $courses->setStudentPresence("Futur");
        }

        $coursesList = array_merge($listPresent, $listAbsent, $listFutur);
        usort($coursesList, function ($a, $b) {
            return $a->getBeginDate() > $b->getBeginDate();
        });

        $view_param = [
            'coursesList' => $coursesList,
        ];
        return $this->render('courses/courses.html.twig', $view_param);
    }
}
