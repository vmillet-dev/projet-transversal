<?php

namespace App\Controller;

use App\Entity\InformationStudent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\StudentGroup;
use App\Entity\Classes;
use App\Entity\QrCode;
use DateTime;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class TeacherController extends AbstractController
{
    public function isNotTeacher(): bool
    {
        $function = $this->container->get('security.token_storage')->getToken()
            ->getUser()->getFunction();
        if ($function == null) {
            return true;
        }
        $rolename = $function->getRoleName();
        return $rolename != "Teacher";
    }

    public function error403()
    {
        //throw new AccessDeniedException('Only teacher can see this page');
        $resp = new Response();
        $resp->setStatusCode(Response::HTTP_FORBIDDEN);
        $resp->send();
        return $resp;
    }

    public function error404()
    {
        //throw new NotFoundHttpException("Page Not Found");
        $resp = new Response();
        $resp->setStatusCode(Response::HTTP_NOT_FOUND);
        $resp->send();
        return $resp;
    }

    public function createQrCode(): QrCode
    {
        $qrCodeValue = random_int(0, 1000000);
        $repository = $this->getDoctrine()->getRepository(QrCode::class);
        $qrCode = $repository->addQrCode($qrCodeValue);
        return $qrCode;
    }

    /**
     * @Route("/generateQrCode", name="generateQrCode")
     */
    public function generateQrCode()
    {
        $qrCode = $this->createQrCode();

        $view_param = [
            'qrCodeValue' => $qrCode->getValue(),
        ];
        return $this->render('QrCode/generateQrCode.html.twig', $view_param);
    }

    /**
     * @Route("/createClass", name="createClass")
     */
    public function createClass(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isNotTeacher()) {
            return $this->error403();
        }

        $result = $request->get("StudentList");
        if ($result != null) {
            $repository = $this->getDoctrine()->getRepository(InformationStudent::class);

            $newStudentGroup = new StudentGroup();
            $newStudentGroup->setGroupName($request->get("nameClasses"));
            foreach ($result as $idInformationStudent) {
                $user = $repository->find($idInformationStudent);
                $newStudentGroup->addStuddent($user);
            }

            $repository = $this->getDoctrine()->getRepository(StudentGroup::class);
            $repository->addStudentGroup($newStudentGroup);
        }

        $repository = $this->getDoctrine()->getRepository(InformationStudent::class);
        $userList = $repository->getAllStudent();
        $view_param = [
            'userList' => $userList,
        ];
        return $this->render('teacher/createClass/createClass.html.twig', $view_param);
    }

    /**
     * @Route("/listClass", name="listClass")
     */
    public function listClass()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isNotTeacher()) {
            return $this->error403();
        }

        $repository = $this->getDoctrine()->getRepository(StudentGroup::class);
        $classList = $repository->getAllStudentGroup();
        $view_param = [
            'classList' => $classList,
        ];
        return $this->render('teacher/listClasses/listClasses.html.twig', $view_param);
    }

    /**
     * @Route("/deleteStudentGroup/{idStudentGroup}", name="deleteStudentGroup")
     */
    public function deleteStudentGroup(int $idStudentGroup)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isNotTeacher()) {
            return $this->error403();
        }

        $repository = $this->getDoctrine()->getRepository(StudentGroup::class);
        //if $idStudentGroup is valid
        if ($repository->find($idStudentGroup) != null) {
            $repository->removeStudentGroup($idStudentGroup);
            $resp = new Response();
            $resp->setStatusCode(Response::HTTP_OK);
            $resp->send();
            return $resp;
        } else {
            return $this->error404();
        }
    }

    /**
     * @Route("/createCourses", name="createCourses")
     */
    public function createCourses(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isNotTeacher()) {
            return $this->error403();
        }

        $result = $request->get("ClasseSelected");
        if ($result != null) {
            $repository = $this->getDoctrine()->getRepository(StudentGroup::class);

            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            $beginDateCourses = new DateTime($request->get("beginDateCourses"));
            $endDateCourses = new DateTime($request->get("endDateCourses"));
            $qrCode = $this->createQrCode();

            $newClasses = new Classes();
            $newClasses->setCreator($user);
            $newClasses->setNameUe($request->get("nameCourses"));
            $newClasses->setBeginDate($beginDateCourses);
            $newClasses->setEndDate($endDateCourses);
            $newClasses->setQrCode($qrCode);
            $idStudentGroup = $result;
            $classes = $repository->find($idStudentGroup);
            $newClasses->setStudentGroup($classes);

            $repository = $this->getDoctrine()->getRepository(Classes::class);
            $repository->addClasses($newClasses);
        }

        $repository = $this->getDoctrine()->getRepository(StudentGroup::class);
        $classList = $repository->getAllStudentGroup();
        $view_param = [
            'classList' => $classList,
        ];
        return $this->render('teacher/createCourses/createCourses.html.twig', $view_param);
    }

    /**
     * @Route("/listCourses", name="listCourses")
     */
    public function listCourses()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isNotTeacher()) {
            return $this->error403();
        }

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $repository = $this->getDoctrine()->getRepository(Classes::class);
        $coursesList = $repository->getClassesByCreator($user);
        $view_param = [
            'coursesList' => $coursesList,
        ];
        return $this->render('teacher/listCourses/listCourses.html.twig', $view_param);
    }

    /**
     * @Route("/deleteCourses/{idClasses}", name="deleteCourses")
     */
    public function deleteCourses(int $idClasses)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isNotTeacher()) {
            return $this->error403();
        }

        $repository = $this->getDoctrine()->getRepository(Classes::class);
        //if $idClasses is valid
        if ($repository->find($idClasses) != null) {
            $repository->removeClasse($idClasses);
            $resp = new Response();
            $resp->setStatusCode(Response::HTTP_OK);
            $resp->send();
            return $resp;
        } else {
            return $this->error404();
        }
    }

    /**
     * @Route("/displayQrCode/{qrCodeValue}", name="displayQrCode")
     */
    public function displayQrCode(int $qrCodeValue)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isNotTeacher()) {
            return $this->error403();
        }
        $view_param = [
            'qrCodeValue' => $qrCodeValue,
        ];
        return $this->render('QrCode/displayQrCode.html.twig', $view_param);
    }
}
