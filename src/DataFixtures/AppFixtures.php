<?php

namespace App\DataFixtures;

use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Role;
use App\Entity\StudentGroup;
use App\Entity\Classes;
use App\Entity\QrCode;
use App\Entity\InformationAdministrator;
use App\Entity\InformationCompany;
use App\Entity\InformationStudent;
use App\Entity\InformationTeacher;
use App\Entity\ValidationPresence;
use Exception;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        //Role
        $roleAdmin = new Role();
        $roleAdmin->setRoleName("Administrator");
        $roleStudent = new Role();
        $roleStudent->setRoleName("Student");
        $roleTeacher = new Role();
        $roleTeacher->setRoleName("Teacher");
        $roleCompany = new Role();
        $roleCompany->setRoleName("Company");

        //Instantiation of studentGroup
        $newStudentGroupA = new StudentGroup();
        $newStudentGroupB = new StudentGroup();

        //Instantiation of Information* classes
        $infAdmin = new InformationAdministrator();

        $infCompanyTeck = new InformationCompany();
        $infCompanyCPP = new InformationCompany();

        $infStudent = new InformationStudent();
        $infStudentMatheo = new InformationStudent();
        $infStudentBaptiste = new InformationStudent();
        $infStudentMarie = new InformationStudent();
        $infStudentFlorent = new InformationStudent();
        $infStudentVictor = new InformationStudent();
        $infStudentQuentin = new InformationStudent();
        $infStudentSarah = new InformationStudent();
        $infStudentLouis = new InformationStudent();
        $infStudentMyriam = new InformationStudent();
        $infStudentDaniel = new InformationStudent();
        $infStudentAyoub = new InformationStudent();
        $infStudentLaura = new InformationStudent();
        $infStudentRomain = new InformationStudent();
        $infStudentAmaury = new InformationStudent();
        $infStudentMorgane = new InformationStudent();
        $infStudentLeo = new InformationStudent();
        $infStudentSophie = new InformationStudent();


        $infTeacher1 = new InformationTeacher();
        $infTeacher2 = new InformationTeacher();

        //Users
        ////User Admin
        $userAdmin = new User();
        $userAdmin->setSuperAdmin(true);
        $userAdmin->setInformationAdministrator($infAdmin);
        $userAdmin->setFunction($roleAdmin);
        $userAdmin->setPassword($this->encoder->encodePassword($userAdmin, "admin"));
        $userAdmin->setUsername("admin");
        $userAdmin->setUsernameCanonical("admin");
        $userAdmin->setEnabled(true);
        $userAdmin->setEmail("admin@univ.fr");
        $userAdmin->setEmailCanonical("admin@univ.fr");

        ////Users Company
        $userCompanyTeck = new User();
        $userCompanyTeck->setFunction($roleCompany);
        $userCompanyTeck->setInformationCompany($infCompanyTeck);
        $userCompanyTeck->setPassword($this->encoder->encodePassword($userCompanyTeck, "company"));
        $userCompanyTeck->setUsername("company");
        $userCompanyTeck->setUsernameCanonical("company");
        $userCompanyTeck->setEnabled(true);
        $userCompanyTeck->setEmail("contact@tecknet.com");
        $userCompanyTeck->setEmailCanonical("contact@tecknet.com");

        $userCompanyCPP = new User();
        $userCompanyCPP->setFunction($roleCompany);
        $userCompanyCPP->setInformationCompany($infCompanyCPP);
        $userCompanyCPP->setPassword($this->encoder->encodePassword($userCompanyCPP, "company"));
        $userCompanyCPP->setUsername("companyCPP");
        $userCompanyCPP->setUsernameCanonical("companyCPP");
        $userCompanyCPP->setEnabled(true);
        $userCompanyCPP->setEmail("contact@cpplus.com");
        $userCompanyCPP->setEmailCanonical("contact@cpplus.com");

        ////Users Student with role
        $userStudent = new User();
        $userStudent->setFunction($roleStudent);
        $userStudent->setInformationStudent($infStudent);
        $userStudent->setPassword($this->encoder->encodePassword($userStudent, "student"));
        $userStudent->setUsername("student");
        $userStudent->setUsernameCanonical("student");
        $userStudent->setEnabled(true);
        $userStudent->setEmail("student@univ.fr");
        $userStudent->setEmailCanonical("student@univ.fr");

        $userStudentMatheo = new User();
        $userStudentMatheo->setFunction($roleStudent);
        $userStudentMatheo->setInformationStudent($infStudentMatheo);
        $userStudentMatheo->setPassword($this->encoder->encodePassword($userStudentMatheo, "student"));
        $userStudentMatheo->setUsername("Matheo");
        $userStudentMatheo->setUsernameCanonical("Matheo");
        $userStudentMatheo->setEnabled(true);
        $userStudentMatheo->setEmail("matheo@univ.fr");
        $userStudentMatheo->setEmailCanonical("matheo@univ.fr");

        $userStudentBaptiste = new User();
        $userStudentBaptiste->setFunction($roleStudent);
        $userStudentBaptiste->setInformationStudent($infStudentBaptiste);
        $userStudentBaptiste->setPassword($this->encoder->encodePassword($userStudentBaptiste, "student"));
        $userStudentBaptiste->setUsername("Baptise");
        $userStudentBaptiste->setUsernameCanonical("Baptise");
        $userStudentBaptiste->setEnabled(true);
        $userStudentBaptiste->setEmail("baptise@univ.fr");
        $userStudentBaptiste->setEmailCanonical("baptise@univ.fr");

        $userStudentMarie = new User();
        $userStudentMarie->setFunction($roleStudent);
        $userStudentMarie->setInformationStudent($infStudentMarie);
        $userStudentMarie->setPassword($this->encoder->encodePassword($userStudentMarie, "student"));
        $userStudentMarie->setUsername("Marie");
        $userStudentMarie->setUsernameCanonical("Marie");
        $userStudentMarie->setEnabled(true);
        $userStudentMarie->setEmail("marie@univ.fr");
        $userStudentMarie->setEmailCanonical("marie@univ.fr");

        $userStudentFlorent = new User();
        $userStudentFlorent->setFunction($roleStudent);
        $userStudentFlorent->setInformationStudent($infStudentFlorent);
        $userStudentFlorent->setPassword($this->encoder->encodePassword($userStudentFlorent, "student"));
        $userStudentFlorent->setUsername("Florent");
        $userStudentFlorent->setUsernameCanonical("Florent");
        $userStudentFlorent->setEnabled(true);
        $userStudentFlorent->setEmail("florent@univ.fr");
        $userStudentFlorent->setEmailCanonical("florent@univ.fr");

        $userStudentVictor = new User();
        $userStudentVictor->setFunction($roleStudent);
        $userStudentVictor->setInformationStudent($infStudentVictor);
        $userStudentVictor->setPassword($this->encoder->encodePassword($userStudentVictor, "student"));
        $userStudentVictor->setUsername("Victor");
        $userStudentVictor->setUsernameCanonical("Victor");
        $userStudentVictor->setEnabled(true);
        $userStudentVictor->setEmail("victor@univ.fr");
        $userStudentVictor->setEmailCanonical("victor@univ.fr");

        $userStudentQuentin = new User();
        $userStudentQuentin->setFunction($roleStudent);
        $userStudentQuentin->setInformationStudent($infStudentQuentin);
        $userStudentQuentin->setPassword($this->encoder->encodePassword($userStudentQuentin, "student"));
        $userStudentQuentin->setUsername("Quentin");
        $userStudentQuentin->setUsernameCanonical("Quentin");
        $userStudentQuentin->setEnabled(true);
        $userStudentQuentin->setEmail("quentin@univ.fr");
        $userStudentQuentin->setEmailCanonical("quentin@univ.fr");

        $userStudentSarah = new User();
        $userStudentSarah->setFunction($roleStudent);
        $userStudentSarah->setInformationStudent($infStudentSarah);
        $userStudentSarah->setPassword($this->encoder->encodePassword($userStudentSarah, "student"));
        $userStudentSarah->setUsername("Sarah");
        $userStudentSarah->setUsernameCanonical("Sarah");
        $userStudentSarah->setEnabled(true);
        $userStudentSarah->setEmail("sarah@univ.fr");
        $userStudentSarah->setEmailCanonical("sarah@univ.fr");

        $userStudentLouis = new User();
        $userStudentLouis->setFunction($roleStudent);
        $userStudentLouis->setInformationStudent($infStudentLouis);
        $userStudentLouis->setPassword($this->encoder->encodePassword($userStudentLouis, "student"));
        $userStudentLouis->setUsername("Louis");
        $userStudentLouis->setUsernameCanonical("Louis");
        $userStudentLouis->setEnabled(true);
        $userStudentLouis->setEmail("louis@univ.fr");
        $userStudentLouis->setEmailCanonical("louis@univ.fr");

        $userStudentMyriam = new User();
        $userStudentMyriam->setFunction($roleStudent);
        $userStudentMyriam->setInformationStudent($infStudentMyriam);
        $userStudentMyriam->setPassword($this->encoder->encodePassword($userStudentMyriam, "student"));
        $userStudentMyriam->setUsername("Myriam");
        $userStudentMyriam->setUsernameCanonical("Myriam");
        $userStudentMyriam->setEnabled(true);
        $userStudentMyriam->setEmail("myriam@univ.fr");
        $userStudentMyriam->setEmailCanonical("myriam@univ.fr");

        $userStudentDaniel = new User();
        $userStudentDaniel->setFunction($roleStudent);
        $userStudentDaniel->setInformationStudent($infStudentDaniel);
        $userStudentDaniel->setPassword($this->encoder->encodePassword($userStudentDaniel, "student"));
        $userStudentDaniel->setUsername("Daniel");
        $userStudentDaniel->setUsernameCanonical("Daniel");
        $userStudentDaniel->setEnabled(true);
        $userStudentDaniel->setEmail("daniel@univ.fr");
        $userStudentDaniel->setEmailCanonical("daniel@univ.fr");

        $userStudentAyoub = new User();
        $userStudentAyoub->setFunction($roleStudent);
        $userStudentAyoub->setInformationStudent($infStudentAyoub);
        $userStudentAyoub->setPassword($this->encoder->encodePassword($userStudentAyoub, "student"));
        $userStudentAyoub->setUsername("Ayoub");
        $userStudentAyoub->setUsernameCanonical("Ayoub");
        $userStudentAyoub->setEnabled(true);
        $userStudentAyoub->setEmail("ayoub@univ.fr");
        $userStudentAyoub->setEmailCanonical("ayoub@univ.fr");

        $userStudentLaura = new User();
        $userStudentLaura->setFunction($roleStudent);
        $userStudentLaura->setInformationStudent($infStudentLaura);
        $userStudentLaura->setPassword($this->encoder->encodePassword($userStudentLaura, "student"));
        $userStudentLaura->setUsername("Laura");
        $userStudentLaura->setUsernameCanonical("Laura");
        $userStudentLaura->setEnabled(true);
        $userStudentLaura->setEmail("laura@univ.fr");
        $userStudentLaura->setEmailCanonical("laura@univ.fr");

        $userStudentRomain = new User();
        $userStudentRomain->setFunction($roleStudent);
        $userStudentRomain->setInformationStudent($infStudentRomain);
        $userStudentRomain->setPassword($this->encoder->encodePassword($userStudentRomain, "student"));
        $userStudentRomain->setUsername("Romain");
        $userStudentRomain->setUsernameCanonical("Romain");
        $userStudentRomain->setEnabled(true);
        $userStudentRomain->setEmail("romain@univ.fr");
        $userStudentRomain->setEmailCanonical("romain@univ.fr");

        $userStudentAmaury = new User();
        $userStudentAmaury->setFunction($roleStudent);
        $userStudentAmaury->setInformationStudent($infStudentAmaury);
        $userStudentAmaury->setPassword($this->encoder->encodePassword($userStudentAmaury, "student"));
        $userStudentAmaury->setUsername("Amaury");
        $userStudentAmaury->setUsernameCanonical("Amaury");
        $userStudentAmaury->setEnabled(true);
        $userStudentAmaury->setEmail("amaury@univ.fr");
        $userStudentAmaury->setEmailCanonical("amaury@univ.fr");

        $userStudentMorgane = new User();
        $userStudentMorgane->setFunction($roleStudent);
        $userStudentMorgane->setInformationStudent($infStudentMorgane);
        $userStudentMorgane->setPassword($this->encoder->encodePassword($userStudentMorgane, "student"));
        $userStudentMorgane->setUsername("Morgane");
        $userStudentMorgane->setUsernameCanonical("Morgane");
        $userStudentMorgane->setEnabled(true);
        $userStudentMorgane->setEmail("morgane@univ.fr");
        $userStudentMorgane->setEmailCanonical("morgane@univ.fr");

        $userStudentLeo = new User();
        $userStudentLeo->setFunction($roleStudent);
        $userStudentLeo->setInformationStudent($infStudentLeo);
        $userStudentLeo->setPassword($this->encoder->encodePassword($userStudentLeo, "student"));
        $userStudentLeo->setUsername("Leo");
        $userStudentLeo->setUsernameCanonical("Leo");
        $userStudentLeo->setEnabled(true);
        $userStudentLeo->setEmail("leo@univ.fr");
        $userStudentLeo->setEmailCanonical("leo@univ.fr");

        $userStudentSophie = new User();
        $userStudentSophie->setFunction($roleStudent);
        $userStudentSophie->setInformationStudent($infStudentSophie);
        $userStudentSophie->setPassword($this->encoder->encodePassword($userStudentSophie, "student"));
        $userStudentSophie->setUsername("Sophie");
        $userStudentSophie->setUsernameCanonical("Sophie");
        $userStudentSophie->setEnabled(true);
        $userStudentSophie->setEmail("sophie@univ.fr");
        $userStudentSophie->setEmailCanonical("sophie@univ.fr");

        ////Users student without role
        $userNoRole = new User();
        $userNoRole->setPassword($this->encoder->encodePassword($userNoRole, "norole"));
        $userNoRole->setUsername("norole1");
        $userNoRole->setUsernameCanonical("norole1");
        $userNoRole->setEnabled(true);
        $userNoRole->setEmail("norole1@univ.fr");
        $userNoRole->setEmailCanonical("norole1@univ.fr");

        $userNoRole2 = new User();
        $userNoRole2->setPassword($this->encoder->encodePassword($userNoRole2, "norole"));
        $userNoRole2->setUsername("NoRole2");
        $userNoRole2->setUsernameCanonical("NoRole2");
        $userNoRole2->setEnabled(true);
        $userNoRole2->setEmail("norole2@univ.fr");
        $userNoRole2->setEmailCanonical("norole2@univ.fr");

        $userNoRole3 = new User();
        $userNoRole3->setPassword($this->encoder->encodePassword($userNoRole3, "norole"));
        $userNoRole3->setUsername("NoRole3");
        $userNoRole3->setUsernameCanonical("NoRole3");
        $userNoRole3->setEnabled(true);
        $userNoRole3->setEmail("norole3@univ.fr");
        $userNoRole3->setEmailCanonical("norole3@univ.fr");

        $userNoRole4 = new User();
        $userNoRole4->setPassword($this->encoder->encodePassword($userNoRole4, "norole"));
        $userNoRole4->setUsername("NoRole4");
        $userNoRole4->setUsernameCanonical("NoRole4");
        $userNoRole4->setEnabled(true);
        $userNoRole4->setEmail("norole4@univ.fr");
        $userNoRole4->setEmailCanonical("norole4@univ.fr");

        $userNoRole5 = new User();
        $userNoRole5->setPassword($this->encoder->encodePassword($userNoRole5, "norole"));
        $userNoRole5->setUsername("NoRole5");
        $userNoRole5->setUsernameCanonical("NoRole5");
        $userNoRole5->setEnabled(true);
        $userNoRole5->setEmail("norole5@univ.fr");
        $userNoRole5->setEmailCanonical("norole5@univ.fr");

        ////Users Teacher
        $userTeacher1 = new User();
        $userTeacher1->setInformationTeacher($infTeacher1);
        $userTeacher1->setFunction($roleTeacher);
        $userTeacher1->setPassword($this->encoder->encodePassword($userTeacher1, "teacher"));
        $userTeacher1->setUsername("teacher");
        $userTeacher1->setUsernameCanonical("teacher");
        $userTeacher1->setEnabled(true);
        $userTeacher1->setEmail("teacher@univ.fr");
        $userTeacher1->setEmailCanonical("teacher@univ.fr");

        $userTeacher2 = new User();
        $userTeacher2->setInformationTeacher($infTeacher2);
        $userTeacher2->setFunction($roleTeacher);
        $userTeacher2->setPassword($this->encoder->encodePassword($userTeacher2, "teacher"));
        $userTeacher2->setUsername("teacher2");
        $userTeacher2->setUsernameCanonical("teacher2");
        $userTeacher2->setEnabled(true);
        $userTeacher2->setEmail("teacher2@univ.fr");
        $userTeacher2->setEmailCanonical("teacher2@univ.fr");

        //InformationAdministrator
        $infAdmin->setUser($userAdmin);

        //InformationCompany
        $infCompanyTeck->setName("Tecknet Inc.");
        $infCompanyTeck->setUser($userCompanyTeck);
        $infCompanyCPP->setName("CPPlus");
        $infCompanyCPP->setUser($userCompanyCPP);

        //InformationStudent
        $infStudent->setUser($userStudent);
        $infStudent->setCompany($infCompanyTeck);
        $infStudent->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudent->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudent->setStudentGroup($newStudentGroupA);

        $infStudentMatheo->setUser($userStudentMatheo);
        $infStudentMatheo->setCompany($infCompanyCPP);
        $infStudentMatheo->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentMatheo->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentMatheo->setStudentGroup($newStudentGroupB);

        $infStudentBaptiste->setUser($userStudentBaptiste);
        $infStudentBaptiste->setCompany($infCompanyTeck);
        $infStudentBaptiste->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentBaptiste->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentBaptiste->setStudentGroup($newStudentGroupB);

        $infStudentMarie->setUser($userStudentMarie);
        $infStudentMarie->setCompany($infCompanyCPP);
        $infStudentMarie->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentMarie->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentMarie->setStudentGroup($newStudentGroupB);

        $infStudentFlorent->setUser($userStudentFlorent);
        $infStudentFlorent->setCompany($infCompanyCPP);
        $infStudentFlorent->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentFlorent->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentFlorent->setStudentGroup($newStudentGroupA);

        $infStudentVictor->setUser($userStudentVictor);
        $infStudentVictor->setCompany($infCompanyCPP);
        $infStudentVictor->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentVictor->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentVictor->setStudentGroup($newStudentGroupA);

        $infStudentQuentin->setUser($userStudentQuentin);
        $infStudentQuentin->setCompany($infCompanyTeck);
        $infStudentQuentin->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentQuentin->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentQuentin->setStudentGroup($newStudentGroupA);

        $infStudentSarah->setUser($userStudentSarah);
        $infStudentSarah->setCompany($infCompanyTeck);
        $infStudentSarah->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentSarah->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentSarah->setStudentGroup($newStudentGroupA);

        $infStudentLouis->setUser($userStudentLouis);
        $infStudentLouis->setCompany($infCompanyCPP);
        $infStudentLouis->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentLouis->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentLouis->setStudentGroup($newStudentGroupB);

        $infStudentMyriam->setUser($userStudentMyriam);
        $infStudentMyriam->setCompany($infCompanyCPP);
        $infStudentMyriam->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentMyriam->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentMyriam->setStudentGroup($newStudentGroupA);

        $infStudentDaniel->setUser($userStudentDaniel);
        $infStudentDaniel->setCompany($infCompanyCPP);
        $infStudentDaniel->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentDaniel->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentDaniel->setStudentGroup($newStudentGroupA);

        $infStudentAyoub->setUser($userStudentAyoub);
        $infStudentAyoub->setCompany($infCompanyCPP);
        $infStudentAyoub->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentAyoub->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentAyoub->setStudentGroup($newStudentGroupB);

        $infStudentLaura->setUser($userStudentLaura);
        $infStudentLaura->setCompany($infCompanyTeck);
        $infStudentLaura->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentLaura->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentLaura->setStudentGroup($newStudentGroupB);

        $infStudentRomain->setUser($userStudentRomain);
        $infStudentRomain->setCompany($infCompanyTeck);
        $infStudentRomain->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentRomain->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentRomain->setStudentGroup($newStudentGroupB);

        $infStudentAmaury->setUser($userStudentAmaury);
        $infStudentAmaury->setCompany($infCompanyTeck);
        $infStudentAmaury->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentAmaury->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentAmaury->setStudentGroup($newStudentGroupA);

        $infStudentMorgane->setUser($userStudentMorgane);
        $infStudentMorgane->setCompany($infCompanyTeck);
        $infStudentMorgane->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentMorgane->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentMorgane->setStudentGroup($newStudentGroupA);

        $infStudentLeo->setUser($userStudentLeo);
        $infStudentLeo->setCompany($infCompanyCPP);
        $infStudentLeo->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentLeo->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentLeo->setStudentGroup($newStudentGroupA);

        $infStudentSophie->setUser($userStudentSophie);
        $infStudentSophie->setCompany($infCompanyCPP);
        $infStudentSophie->setStartDateContrat(new DateTime('2019-09-09 08:00:00'));
        $infStudentSophie->setEndDateContrat(new DateTime('2020-07-31 18:00:00'));
        $infStudentSophie->setStudentGroup($newStudentGroupB);

        //InformationTeacher
        $infTeacher1->setUser($userTeacher1);
        $infTeacher1->addStudentGroup($newStudentGroupA);
        $infTeacher1->addStudentGroup($newStudentGroupB);
        $infTeacher2->setUser($userTeacher2);
        $infTeacher2->addStudentGroup($newStudentGroupB);

        //Initialisation of classes
        $newClasses1 = new Classes();
        $newClasses2 = new Classes();
        $newClasses3 = new Classes();
        $newClasses4 = new Classes();
        $newClasses5 = new Classes();
        $newClasses6 = new Classes();
        $newClasses7 = new Classes();
        $newClasses8 = new Classes();

        //StudentGroup
        $newStudentGroupA->setGroupName("Group A");
        $newStudentGroupA->setCreatorTeacher($infTeacher1);
        $newStudentGroupA->addStuddent($infStudent);
        $newStudentGroupA->addCourse($newClasses1);
        $newStudentGroupA->addCourse($newClasses3);
        $newStudentGroupA->addCourse($newClasses5);
        $newStudentGroupA->addCourse($newClasses7);
        $newStudentGroupB->setGroupName("Group B");
        $newStudentGroupB->setCreatorTeacher($infTeacher2);
        $newStudentGroupB->addStuddent($infStudentMatheo);
        $newStudentGroupB->addCourse($newClasses2);
        $newStudentGroupB->addCourse($newClasses4);
        $newStudentGroupB->addCourse($newClasses6);
        $newStudentGroupB->addCourse($newClasses8);

        //QrCode
        $qrCode1 = new QrCode();
        $qrCode2 = new QrCode();
        $qrCode3 = new QrCode();
        $qrCode4 = new QrCode();
        $qrCode5 = new QrCode();
        $qrCode6 = new QrCode();
        $qrCode7 = new QrCode();
        $qrCode8 = new QrCode();

        try {
            $qrCode1->setValue(111111);
            $qrCode2->setValue(222222);
            $qrCode3->setValue(333333);
            $qrCode4->setValue(444444);
            $qrCode5->setValue(555555);
            $qrCode6->setValue(666666);
            $qrCode7->setValue(777777);
            $qrCode8->setValue(888888);
        } catch (Exception $e) {
            // Do nothing
        }

        //ValidationPresence : Classe1

        $vPresenceClasse1Student1 = new ValidationPresence();
        $vPresenceClasse1Student1->setClasses($newClasses1);
        $vPresenceClasse1Student1->setStudents($infStudent);

        $vPresenceClasse1Student2 = new ValidationPresence();
        $vPresenceClasse1Student2->setClasses($newClasses1);
        $vPresenceClasse1Student2->setStudents($infStudentFlorent);

        $vPresenceClasse1Student3 = new ValidationPresence();
        $vPresenceClasse1Student3->setClasses($newClasses1);
        $vPresenceClasse1Student3->setStudents($infStudentVictor);

        $vPresenceClasse1Student4 = new ValidationPresence();
        $vPresenceClasse1Student4->setClasses($newClasses1);
        $vPresenceClasse1Student4->setStudents($infStudentQuentin);

        $vPresenceClasse1Student5 = new ValidationPresence();
        $vPresenceClasse1Student5->setClasses($newClasses1);
        $vPresenceClasse1Student5->setStudents($infStudentSarah);

        $vPresenceClasse1Student5 = new ValidationPresence();
        $vPresenceClasse1Student5->setClasses($newClasses1);
        $vPresenceClasse1Student5->setStudents($infStudentMyriam);

        $vPresenceClasse1Student6 = new ValidationPresence();
        $vPresenceClasse1Student6->setClasses($newClasses1);
        $vPresenceClasse1Student6->setStudents($infStudentDaniel);

        $vPresenceClasse1Student7 = new ValidationPresence();
        $vPresenceClasse1Student7->setClasses($newClasses1);
        $vPresenceClasse1Student7->setStudents($infStudentAmaury);

        $vPresenceClasse1Student8 = new ValidationPresence();
        $vPresenceClasse1Student8->setClasses($newClasses1);
        $vPresenceClasse1Student8->setStudents($infStudentMorgane);

        $vPresenceClasse1Student9 = new ValidationPresence();
        $vPresenceClasse1Student9->setClasses($newClasses1);
        $vPresenceClasse1Student9->setStudents($infStudentLeo);
        
        //ValidationPresence : Classe2

        $vPresenceClasse2Student1 = new ValidationPresence();
        $vPresenceClasse2Student1->setClasses($newClasses2);
        $vPresenceClasse2Student1->setStudents($infStudentMatheo);

        $vPresenceClasse2Student2 = new ValidationPresence();
        $vPresenceClasse2Student2->setClasses($newClasses2);
        $vPresenceClasse2Student2->setStudents($infStudentBaptiste);

        $vPresenceClasse2Student3 = new ValidationPresence();
        $vPresenceClasse2Student3->setClasses($newClasses2);
        $vPresenceClasse2Student3->setStudents($infStudentMarie);

        $vPresenceClasse2Student4 = new ValidationPresence();
        $vPresenceClasse2Student4->setClasses($newClasses2);
        $vPresenceClasse2Student4->setStudents($infStudentLouis);

        $vPresenceClasse2Student5 = new ValidationPresence();
        $vPresenceClasse2Student5->setClasses($newClasses2);
        $vPresenceClasse2Student5->setStudents($infStudentAyoub);

        $vPresenceClasse2Student5 = new ValidationPresence();
        $vPresenceClasse2Student5->setClasses($newClasses2);
        $vPresenceClasse2Student5->setStudents($infStudentLaura);

        $vPresenceClasse2Student6 = new ValidationPresence();
        $vPresenceClasse2Student6->setClasses($newClasses2);
        $vPresenceClasse2Student6->setStudents($infStudentRomain);

        $vPresenceClasse2Student7 = new ValidationPresence();
        $vPresenceClasse2Student7->setClasses($newClasses2);
        $vPresenceClasse2Student7->setStudents($infStudentSophie);

        //ValidationPresence : Classe3

        $vPresenceClasse3Student1 = new ValidationPresence();
        $vPresenceClasse3Student1->setClasses($newClasses3);
        $vPresenceClasse3Student1->setStudents($infStudent);

        $vPresenceClasse3Student2 = new ValidationPresence();
        $vPresenceClasse3Student2->setClasses($newClasses3);
        $vPresenceClasse3Student2->setStudents($infStudentFlorent);

        $vPresenceClasse3Student3 = new ValidationPresence();
        $vPresenceClasse3Student3->setClasses($newClasses3);
        $vPresenceClasse3Student3->setStudents($infStudentVictor);

        $vPresenceClasse3Student4 = new ValidationPresence();
        $vPresenceClasse3Student4->setClasses($newClasses3);
        $vPresenceClasse3Student4->setStudents($infStudentQuentin);

        $vPresenceClasse3Student5 = new ValidationPresence();
        $vPresenceClasse3Student5->setClasses($newClasses3);
        $vPresenceClasse3Student5->setStudents($infStudentSarah);

        $vPresenceClasse3Student5 = new ValidationPresence();
        $vPresenceClasse3Student5->setClasses($newClasses3);
        $vPresenceClasse3Student5->setStudents($infStudentMyriam);

        $vPresenceClasse3Student6 = new ValidationPresence();
        $vPresenceClasse3Student6->setClasses($newClasses3);
        $vPresenceClasse3Student6->setStudents($infStudentDaniel);

        $vPresenceClasse3Student7 = new ValidationPresence();
        $vPresenceClasse3Student7->setClasses($newClasses3);
        $vPresenceClasse3Student7->setStudents($infStudentAmaury);

        $vPresenceClasse3Student8 = new ValidationPresence();
        $vPresenceClasse3Student8->setClasses($newClasses3);
        $vPresenceClasse3Student8->setStudents($infStudentMorgane);

        $vPresenceClasse3Student9 = new ValidationPresence();
        $vPresenceClasse3Student9->setClasses($newClasses3);
        $vPresenceClasse3Student9->setStudents($infStudentLeo);

        //ValidationPresence : Classe4

        $vPresenceClasse4Student1 = new ValidationPresence();
        $vPresenceClasse4Student1->setClasses($newClasses4);
        $vPresenceClasse4Student1->setStudents($infStudentMatheo);

        $vPresenceClasse4Student2 = new ValidationPresence();
        $vPresenceClasse4Student2->setClasses($newClasses4);
        $vPresenceClasse4Student2->setStudents($infStudentBaptiste);

        $vPresenceClasse4Student3 = new ValidationPresence();
        $vPresenceClasse4Student3->setClasses($newClasses4);
        $vPresenceClasse4Student3->setStudents($infStudentMarie);

        $vPresenceClasse4Student4 = new ValidationPresence();
        $vPresenceClasse4Student4->setClasses($newClasses4);
        $vPresenceClasse4Student4->setStudents($infStudentLouis);

        $vPresenceClasse4Student5 = new ValidationPresence();
        $vPresenceClasse4Student5->setClasses($newClasses4);
        $vPresenceClasse4Student5->setStudents($infStudentAyoub);

        $vPresenceClasse4Student5 = new ValidationPresence();
        $vPresenceClasse4Student5->setClasses($newClasses4);
        $vPresenceClasse4Student5->setStudents($infStudentLaura);

        $vPresenceClasse4Student6 = new ValidationPresence();
        $vPresenceClasse4Student6->setClasses($newClasses4);
        $vPresenceClasse4Student6->setStudents($infStudentRomain);

        $vPresenceClasse4Student7 = new ValidationPresence();
        $vPresenceClasse4Student7->setClasses($newClasses4);
        $vPresenceClasse4Student7->setStudents($infStudentSophie);

        //ValidationPresence : Classe5

        $vPresenceClasse5Student1 = new ValidationPresence();
        $vPresenceClasse5Student1->setClasses($newClasses5);
        $vPresenceClasse5Student1->setStudents($infStudent);

        $vPresenceClasse5Student2 = new ValidationPresence();
        $vPresenceClasse5Student2->setClasses($newClasses5);
        $vPresenceClasse5Student2->setStudents($infStudentFlorent);

        $vPresenceClasse5Student3 = new ValidationPresence();
        $vPresenceClasse5Student3->setClasses($newClasses5);
        $vPresenceClasse5Student3->setStudents($infStudentVictor);

        $vPresenceClasse5Student4 = new ValidationPresence();
        $vPresenceClasse5Student4->setClasses($newClasses5);
        $vPresenceClasse5Student4->setStudents($infStudentQuentin);

        $vPresenceClasse5Student5 = new ValidationPresence();
        $vPresenceClasse5Student5->setClasses($newClasses5);
        $vPresenceClasse5Student5->setStudents($infStudentSarah);

        $vPresenceClasse5Student5 = new ValidationPresence();
        $vPresenceClasse5Student5->setClasses($newClasses5);
        $vPresenceClasse5Student5->setStudents($infStudentMyriam);

        $vPresenceClasse5Student6 = new ValidationPresence();
        $vPresenceClasse5Student6->setClasses($newClasses5);
        $vPresenceClasse5Student6->setStudents($infStudentDaniel);

        $vPresenceClasse5Student7 = new ValidationPresence();
        $vPresenceClasse5Student7->setClasses($newClasses5);
        $vPresenceClasse5Student7->setStudents($infStudentAmaury);

        $vPresenceClasse5Student8 = new ValidationPresence();
        $vPresenceClasse5Student8->setClasses($newClasses5);
        $vPresenceClasse5Student8->setStudents($infStudentMorgane);

        $vPresenceClasse5Student9 = new ValidationPresence();
        $vPresenceClasse5Student9->setClasses($newClasses5);
        $vPresenceClasse5Student9->setStudents($infStudentLeo);

        //ValidationPresence : Classe6
        
        $vPresenceClasse6Student1 = new ValidationPresence();
        $vPresenceClasse6Student1->setClasses($newClasses6);
        $vPresenceClasse6Student1->setStudents($infStudentMatheo);

        $vPresenceClasse6Student2 = new ValidationPresence();
        $vPresenceClasse6Student2->setClasses($newClasses6);
        $vPresenceClasse6Student2->setStudents($infStudentBaptiste);

        $vPresenceClasse6Student3 = new ValidationPresence();
        $vPresenceClasse6Student3->setClasses($newClasses6);
        $vPresenceClasse6Student3->setStudents($infStudentMarie);

        $vPresenceClasse6Student4 = new ValidationPresence();
        $vPresenceClasse6Student4->setClasses($newClasses6);
        $vPresenceClasse6Student4->setStudents($infStudentLouis);

        $vPresenceClasse6Student5 = new ValidationPresence();
        $vPresenceClasse6Student5->setClasses($newClasses6);
        $vPresenceClasse6Student5->setStudents($infStudentAyoub);

        $vPresenceClasse6Student5 = new ValidationPresence();
        $vPresenceClasse6Student5->setClasses($newClasses6);
        $vPresenceClasse6Student5->setStudents($infStudentLaura);

        $vPresenceClasse6Student6 = new ValidationPresence();
        $vPresenceClasse6Student6->setClasses($newClasses6);
        $vPresenceClasse6Student6->setStudents($infStudentRomain);

        $vPresenceClasse6Student7 = new ValidationPresence();
        $vPresenceClasse6Student7->setClasses($newClasses6);
        $vPresenceClasse6Student7->setStudents($infStudentSophie);

        //ValidationPresence : Classe7

        $vPresenceClasse7Student1 = new ValidationPresence();
        $vPresenceClasse7Student1->setClasses($newClasses7);
        $vPresenceClasse7Student1->setStudents($infStudent);

        $vPresenceClasse7Student2 = new ValidationPresence();
        $vPresenceClasse7Student2->setClasses($newClasses7);
        $vPresenceClasse7Student2->setStudents($infStudentFlorent);

        $vPresenceClasse7Student3 = new ValidationPresence();
        $vPresenceClasse7Student3->setClasses($newClasses7);
        $vPresenceClasse7Student3->setStudents($infStudentVictor);

        $vPresenceClasse7Student4 = new ValidationPresence();
        $vPresenceClasse7Student4->setClasses($newClasses7);
        $vPresenceClasse7Student4->setStudents($infStudentQuentin);

        $vPresenceClasse7Student5 = new ValidationPresence();
        $vPresenceClasse7Student5->setClasses($newClasses7);
        $vPresenceClasse7Student5->setStudents($infStudentSarah);

        $vPresenceClasse7Student5 = new ValidationPresence();
        $vPresenceClasse7Student5->setClasses($newClasses7);
        $vPresenceClasse7Student5->setStudents($infStudentMyriam);

        $vPresenceClasse7Student6 = new ValidationPresence();
        $vPresenceClasse7Student6->setClasses($newClasses7);
        $vPresenceClasse7Student6->setStudents($infStudentDaniel);

        $vPresenceClasse7Student7 = new ValidationPresence();
        $vPresenceClasse7Student7->setClasses($newClasses7);
        $vPresenceClasse7Student7->setStudents($infStudentAmaury);

        $vPresenceClasse7Student8 = new ValidationPresence();
        $vPresenceClasse7Student8->setClasses($newClasses7);
        $vPresenceClasse7Student8->setStudents($infStudentMorgane);

        $vPresenceClasse7Student9 = new ValidationPresence();
        $vPresenceClasse7Student9->setClasses($newClasses7);
        $vPresenceClasse7Student9->setStudents($infStudentLeo);

        //ValidationPresence : Classe8

        $vPresenceClasse8Student1 = new ValidationPresence();
        $vPresenceClasse8Student1->setClasses($newClasses8);
        $vPresenceClasse8Student1->setStudents($infStudentMatheo);

        $vPresenceClasse8Student2 = new ValidationPresence();
        $vPresenceClasse8Student2->setClasses($newClasses8);
        $vPresenceClasse8Student2->setStudents($infStudentBaptiste);

        $vPresenceClasse8Student3 = new ValidationPresence();
        $vPresenceClasse8Student3->setClasses($newClasses8);
        $vPresenceClasse8Student3->setStudents($infStudentMarie);

        $vPresenceClasse8Student4 = new ValidationPresence();
        $vPresenceClasse8Student4->setClasses($newClasses8);
        $vPresenceClasse8Student4->setStudents($infStudentLouis);

        $vPresenceClasse8Student5 = new ValidationPresence();
        $vPresenceClasse8Student5->setClasses($newClasses8);
        $vPresenceClasse8Student5->setStudents($infStudentAyoub);

        $vPresenceClasse8Student5 = new ValidationPresence();
        $vPresenceClasse8Student5->setClasses($newClasses8);
        $vPresenceClasse8Student5->setStudents($infStudentLaura);

        $vPresenceClasse8Student6 = new ValidationPresence();
        $vPresenceClasse8Student6->setClasses($newClasses8);
        $vPresenceClasse8Student6->setStudents($infStudentRomain);

        $vPresenceClasse8Student7 = new ValidationPresence();
        $vPresenceClasse8Student7->setClasses($newClasses8);
        $vPresenceClasse8Student7->setStudents($infStudentSophie);

        //Classes
        $newClasses1->setBeginDate(new DateTime('2019-10-24 08:00:00'));
        $newClasses1->setEndDate(new DateTime('2019-10-24 10:00:00'));
        $newClasses1->setNameUe("M1INF01 - PHP");
        $newClasses1->setQrCode($qrCode1);
        $newClasses1->addPresence($vPresenceClasse1Student1);
        $newClasses1->setCreator($userTeacher1);
        $newClasses1->setStudentGroup($newStudentGroupA);

        $newClasses2->setBeginDate(new DateTime('2019-10-24 13:00:00'));
        $newClasses2->setEndDate(new DateTime('2019-10-24 18:00:00'));
        $newClasses2->setNameUe("M1INF02 - Java");
        $newClasses2->setQrCode($qrCode2);
        $newClasses2->addPresence($vPresenceClasse2Student1);
        $newClasses2->setCreator($userTeacher2);
        $newClasses2->setStudentGroup($newStudentGroupB);

        $newClasses3->setBeginDate(new DateTime('2019-10-24 13:00:00'));
        $newClasses3->setEndDate(new DateTime('2020-10-24 16:30:00'));
        $newClasses3->setNameUe("M1INF04 - C++");
        $newClasses3->setQrCode($qrCode3);
        $newClasses3->addPresence($vPresenceClasse1Student1);
        $newClasses3->setCreator($userTeacher1);
        $newClasses3->setStudentGroup($newStudentGroupA);

        $newClasses4->setBeginDate(new DateTime('2019-10-24 08:00:00'));
        $newClasses4->setEndDate(new DateTime('2019-10-24 12:00:00'));
        $newClasses4->setNameUe("M1INF05 - Network");
        $newClasses4->setQrCode($qrCode4);
        $newClasses4->addPresence($vPresenceClasse1Student1);
        $newClasses4->setCreator($userTeacher1);
        $newClasses4->setStudentGroup($newStudentGroupB);

        $newClasses5->setBeginDate(new DateTime('2019-10-25 08:00:00'));
        $newClasses5->setEndDate(new DateTime('2019-10-25 10:00:00'));
        $newClasses5->setNameUe("M1INF06 - RESTful server and web api");
        $newClasses5->setQrCode($qrCode5);
        $newClasses5->addPresence($vPresenceClasse1Student1);
        $newClasses5->setCreator($userTeacher1);
        $newClasses5->setStudentGroup($newStudentGroupA);

        $newClasses6->setBeginDate(new DateTime('2019-10-25 08:00:00'));
        $newClasses6->setEndDate(new DateTime('2019-10-25 12:00:00'));
        $newClasses6->setNameUe("M1INF07 - Maths");
        $newClasses6->setQrCode($qrCode6);
        $newClasses6->addPresence($vPresenceClasse1Student1);
        $newClasses6->setCreator($userTeacher2);
        $newClasses6->setStudentGroup($newStudentGroupB);

        $newClasses7->setBeginDate(new DateTime('2019-10-25 13:45:00'));
        $newClasses7->setEndDate(new DateTime('2019-10-25 16:00:00'));
        $newClasses7->setNameUe("M1INF08 - English");
        $newClasses7->setQrCode($qrCode7);
        $newClasses7->addPresence($vPresenceClasse1Student1);
        $newClasses7->setCreator($userTeacher1);
        $newClasses7->setStudentGroup($newStudentGroupA);

        $newClasses8->setBeginDate(new DateTime('2019-10-25 13:00:00'));
        $newClasses8->setEndDate(new DateTime('2019-10-25 17:00:00'));
        $newClasses8->setNameUe("M1INF01 - AI And Deep Learning");
        $newClasses8->setQrCode($qrCode8);
        $newClasses8->addPresence($vPresenceClasse1Student1);
        $newClasses8->setCreator($userTeacher2);
        $newClasses8->setStudentGroup($newStudentGroupB);

        /**
         * Tells the ObjectManager to make an instance managed and persistent.
         */

        //Make Role instances => ok
        $manager->persist($roleCompany);
        $manager->persist($roleStudent);
        $manager->persist($roleTeacher);
        $manager->persist($roleAdmin);

        //Make User instances => ok
        $manager->persist($userCompanyTeck);
        $manager->persist($userCompanyCPP);

        $manager->persist($userStudent);
        $manager->persist($userStudentMatheo);
        $manager->persist($userStudentBaptiste);
        $manager->persist($userStudentMarie);
        $manager->persist($userStudentFlorent);
        $manager->persist($userStudentVictor);
        $manager->persist($userStudentQuentin);
        $manager->persist($userStudentSarah);
        $manager->persist($userStudentLouis);
        $manager->persist($userStudentMyriam);
        $manager->persist($userStudentDaniel);
        $manager->persist($userStudentAyoub);
        $manager->persist($userStudentLaura);
        $manager->persist($userStudentRomain);
        $manager->persist($userStudentAmaury);
        $manager->persist($userStudentMorgane);
        $manager->persist($userStudentLeo);
        $manager->persist($userStudentSophie);

        $manager->persist($userNoRole);
        $manager->persist($userNoRole2);
        $manager->persist($userNoRole3);
        $manager->persist($userNoRole4);
        $manager->persist($userNoRole5);

        $manager->persist($userTeacher1);
        $manager->persist($userTeacher2);

        $manager->persist($userAdmin);

        //Make InformationAdministrator instances => ok
        $manager->persist($infAdmin);

        //Make InformationCompany instances => ok
        $manager->persist($infCompanyTeck);
        $manager->persist($infCompanyCPP);

        //Make InformationStudent instances => ok
        $manager->persist($infStudent);
        $manager->persist($infStudentMatheo);
        $manager->persist($infStudentBaptiste);
        $manager->persist($infStudentMarie);
        $manager->persist($infStudentFlorent);
        $manager->persist($infStudentVictor);
        $manager->persist($infStudentQuentin);
        $manager->persist($infStudentSarah);
        $manager->persist($infStudentLouis);
        $manager->persist($infStudentMyriam);
        $manager->persist($infStudentDaniel);
        $manager->persist($infStudentAyoub);
        $manager->persist($infStudentLaura);
        $manager->persist($infStudentRomain);
        $manager->persist($infStudentAmaury);
        $manager->persist($infStudentMorgane);
        $manager->persist($infStudentLeo);
        $manager->persist($infStudentSophie);

        //Make InformationTeacher instances => ok
        $manager->persist($infTeacher1);
        $manager->persist($infTeacher2);

        //Make ValidationPresence instances
        $manager->persist($vPresenceClasse1Student1);
        $manager->persist($vPresenceClasse1Student2);
        $manager->persist($vPresenceClasse1Student3);
        $manager->persist($vPresenceClasse1Student4);
        $manager->persist($vPresenceClasse1Student5);
        $manager->persist($vPresenceClasse1Student6);
        $manager->persist($vPresenceClasse1Student7);
        $manager->persist($vPresenceClasse1Student8);
        $manager->persist($vPresenceClasse1Student9);

        $manager->persist($vPresenceClasse2Student1);
        $manager->persist($vPresenceClasse2Student2);
        $manager->persist($vPresenceClasse2Student3);
        $manager->persist($vPresenceClasse2Student4);
        $manager->persist($vPresenceClasse2Student5);
        $manager->persist($vPresenceClasse2Student6);
        $manager->persist($vPresenceClasse2Student7);

        //$manager->persist($vPresenceClasse3Student1);
        $manager->persist($vPresenceClasse3Student2);
        $manager->persist($vPresenceClasse3Student3);
        $manager->persist($vPresenceClasse3Student4);
        $manager->persist($vPresenceClasse3Student5);
        $manager->persist($vPresenceClasse3Student6);
        $manager->persist($vPresenceClasse3Student7);
        $manager->persist($vPresenceClasse3Student8);
        $manager->persist($vPresenceClasse3Student9);

        $manager->persist($vPresenceClasse4Student1);
        $manager->persist($vPresenceClasse4Student2);
        $manager->persist($vPresenceClasse4Student3);
        $manager->persist($vPresenceClasse4Student4);
        $manager->persist($vPresenceClasse4Student5);
        $manager->persist($vPresenceClasse4Student6);
        $manager->persist($vPresenceClasse4Student7);

        $manager->persist($vPresenceClasse5Student1);
        $manager->persist($vPresenceClasse5Student2);
        $manager->persist($vPresenceClasse5Student3);
        $manager->persist($vPresenceClasse5Student4);
        $manager->persist($vPresenceClasse5Student5);
        $manager->persist($vPresenceClasse5Student6);
        $manager->persist($vPresenceClasse5Student7);
        $manager->persist($vPresenceClasse5Student8);
        $manager->persist($vPresenceClasse5Student9);

        $manager->persist($vPresenceClasse6Student1);
        $manager->persist($vPresenceClasse6Student2);
        $manager->persist($vPresenceClasse6Student3);
        $manager->persist($vPresenceClasse6Student4);
        $manager->persist($vPresenceClasse6Student5);
        $manager->persist($vPresenceClasse6Student6);
        $manager->persist($vPresenceClasse6Student7);

        $manager->persist($vPresenceClasse7Student1);
        $manager->persist($vPresenceClasse7Student2);
        $manager->persist($vPresenceClasse7Student3);
        $manager->persist($vPresenceClasse7Student4);
        $manager->persist($vPresenceClasse7Student5);
        $manager->persist($vPresenceClasse7Student6);
        $manager->persist($vPresenceClasse7Student7);
        $manager->persist($vPresenceClasse7Student8);
        $manager->persist($vPresenceClasse7Student9);

        $manager->persist($vPresenceClasse8Student1);
        $manager->persist($vPresenceClasse8Student2);
        $manager->persist($vPresenceClasse8Student3);
        $manager->persist($vPresenceClasse8Student4);
        $manager->persist($vPresenceClasse8Student5);
        $manager->persist($vPresenceClasse8Student6);
        $manager->persist($vPresenceClasse8Student7);

        //Make Classe instances
        $manager->persist($newClasses1);
        $manager->persist($newClasses2);
        $manager->persist($newClasses3);
        $manager->persist($newClasses4);
        $manager->persist($newClasses5);
        $manager->persist($newClasses6);
        $manager->persist($newClasses7);
        $manager->persist($newClasses8);

        //Make QrCode instances => ok
        $manager->persist($qrCode1);
        $manager->persist($qrCode2);
        $manager->persist($qrCode3);
        $manager->persist($qrCode4);
        $manager->persist($qrCode5);
        $manager->persist($qrCode6);
        $manager->persist($qrCode7);
        $manager->persist($qrCode8);

        //Make StudentGroup instances => ok
        $manager->persist($newStudentGroupA);
        $manager->persist($newStudentGroupB);

        /**
         * Flushes all changes to objects that have been queued up to now to the database.
         */
        $manager->flush();
    }
}
