<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClassesRepository")
 */
class Classes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $beginDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endDate;

    /**
     * @ORM\Column(type="string")
     */
    private $nameUe;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\QrCode")
     */
    private $QrCode;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="courseCreated")
     */
    private $creator;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ValidationPresence", cascade={"persist", "remove"}, mappedBy="classes")
     */
    private $presence;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\StudentGroup", inversedBy="courses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $studentGroup;

    // It's not a bdd parameter, it's just indicate at the view,
    // if a student was present or absent or the classes is in the futur
    private $studentPresence;

    public function getStudentPresence(): string
    {
        return $this->studentPresence;
    }

    public function setStudentPresence(string $studentPresence): self
    {
        $this->studentPresence = $studentPresence;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameUe(): ?string
    {
        return $this->nameUe;
    }

    public function setNameUe(string $nameUe)
    {
        $this->nameUe = $nameUe;
    }

    public function getBeginDate(): ?DateTime
    {
        return $this->beginDate;
    }

    public function setBeginDate(DateTime $beginDate): self
    {
        $this->beginDate = $beginDate;

        return $this;
    }

    public function getEndDate(): ?DateTime
    {
        return $this->endDate;
    }

    public function setEndDate(DateTime $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }


    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getQrCode(): QrCode
    {
        return $this->QrCode;
    }

    public function setQrCode(QrCode $qrCode): self
    {
        $this->QrCode = $qrCode;
        return $this;
    }

    public function getPresence(): Collection
    {
        return $this->presence;
    }

    public function addPresence(ValidationPresence $presence): self
    {

        if ($this->presence != null && !in_array($presence, $this->presence)) {
            $this->presence[] = $presence;
        }
        return $this;
    }

    public function removePresence(ValidationPresence $presence): self
    {
        if ($this->presence->contains($presence)) {
            $this->presence->removeElement($presence);
        }
        return $this;
    }

    public function getStudentGroup(): ?StudentGroup
    {
        return $this->studentGroup;
    }

    public function setStudentGroup(?StudentGroup $studentGroup): self
    {
        $this->studentGroup = $studentGroup;

        return $this;
    }
}
