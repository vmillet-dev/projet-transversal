<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InformationStudentRepository")
 */
class InformationStudent
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startDateContrat;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endDateContrat;

    /**
     * @ORM\ManyToOne(targetEntity="InformationCompany")
     */
    private $company;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="informationStudent")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\StudentGroup", inversedBy="studdents")
     */
    private $studentGroup;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ValidationPresence", cascade={"persist", "remove"}, mappedBy="students")
     */
    private $presence;



    public function __construct()
    {
        // $this->function = new Role();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user): ?self
    {
        $this->user = $user;
        return $this;
    }

    public function getCompany()
    {
        return $this->company;
    }

    public function setCompany($company): ?self
    {
        $this->company = $company;
        return $this;
    }

    public function getStartDateContrat(): ?\DateTime
    {
        return $this->startDateContrat;
    }

    public function setStartDateContrat(\DateTime $startDateContrat): ?self
    {
        $this->startDateContrat = $startDateContrat;
        return $this;
    }

    public function getEndDateContrat(): ?\DateTime
    {
        return $this->endDateContrat;
    }

    public function setEndDateContrat(\DateTime $endDateContrat): ?self
    {
        $this->endDateContrat = $endDateContrat;
        return $this;
    }

    public function getStudentGroup(): ?StudentGroup
    {
        return $this->studentGroup;
    }

    public function setStudentGroup(?StudentGroup $studentGroup): self
    {
        $this->studentGroup = $studentGroup;

        return $this;
    }

    public function getPresence()
    {
        return $this->presence;
    }

    public function addPresence(ValidationPresence $presence): self
    {
        if (!$this->presence->contains($presence)) {
            $this->presence[] = $presence;
        }
        return $this;
    }

    public function removePresence(ValidationPresence $presence): self
    {
        if ($this->studdents->contains($presence)) {
            $this->studdents->removeElement($presence);
        }

        return $this;
    }
}
