<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InformationTeacherRepository")
 */
class InformationTeacher
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", mappedBy="informationTeacher")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StudentGroup", mappedBy="creatorTeacher", orphanRemoval=true)
     */
    private $studentGroups;

    public function __construct()
    {
        $this->studentGroups = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|StudentGroup[]
     */
    public function getStudentGroups(): Collection
    {
        return $this->studentGroups;
    }

    public function addStudentGroup(StudentGroup $studentGroup): self
    {
        if (!$this->studentGroups->contains($studentGroup)) {
            $this->studentGroups[] = $studentGroup;
            $studentGroup->setCreatorTeacher($this);
        }

        return $this;
    }

    public function removeStudentGroup(StudentGroup $studentGroup): self
    {
        if ($this->studentGroups->contains($studentGroup)) {
            $this->studentGroups->removeElement($studentGroup);
            // set the owning side to null (unless already changed)
            if ($studentGroup->getCreatorTeacher() === $this) {
                $studentGroup->setCreatorTeacher(null);
            }
        }

        return $this;
    }
}
