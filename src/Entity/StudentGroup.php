<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StudentGroupRepository")
 */
class StudentGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $groupName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\InformationStudent", mappedBy="studentGroup")
     */
    private $studdents;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\InformationTeacher", inversedBy="studentGroups")
     * @ORM\JoinColumn(nullable=true)
     */
    private $creatorTeacher;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Classes", mappedBy="studentGroup", orphanRemoval=true)
     */
    private $courses;


    public function __construct()
    {
        $this->studdents = new ArrayCollection();
        $this->courses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGroupName(): ?string
    {
        return $this->groupName;
    }

    public function setGroupName(string $groupName): self
    {
        $this->groupName = $groupName;

        return $this;
    }

    /**
     * @return Collection|InformationStudent[]
     */
    public function getStuddents(): Collection
    {
        return $this->studdents;
    }

    public function addStuddent(InformationStudent $studdent): self
    {
        if (!$this->studdents->contains($studdent)) {
            $this->studdents[] = $studdent;
            $studdent->setStudentGroup($this);
        }

        return $this;
    }

    public function removeStuddent(InformationStudent $studdent): self
    {
        if ($this->studdents->contains($studdent)) {
            $this->studdents->removeElement($studdent);
            // set the owning side to null (unless already changed)
            if ($studdent->getStudentGroup() === $this) {
                $studdent->setStudentGroup(null);
            }
        }

        return $this;
    }

    public function getCreatorTeacher(): ?InformationTeacher
    {
        return $this->creatorTeacher;
    }

    public function setCreatorTeacher(?InformationTeacher $creatorTeacher): self
    {
        $this->creatorTeacher = $creatorTeacher;

        return $this;
    }

    /**
     * @return Collection|Classes[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Classes $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setStudentGroup($this);
        }

        return $this;
    }

    public function removeCourse(Classes $course): self
    {
        if ($this->courses->contains($course)) {
            $this->courses->removeElement($course);
            // set the owning side to null (unless already changed)
            if ($course->getStudentGroup() === $this) {
                $course->setStudentGroup(null);
            }
        }

        return $this;
    }
}
