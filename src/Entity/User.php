<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="InformationStudent", cascade={"persist","remove"}, inversedBy="user")
     * @ORM\JoinColumn(name="informationStudent", nullable=true)
     */
    private $informationStudent;

    /**
     * @ORM\OneToOne(targetEntity="InformationCompany", cascade={"persist","remove"}, inversedBy="user")
     * @ORM\JoinColumn(name="informationCompany", nullable=true)
     */
    private $informationCompany;

    /**
     * @ORM\OneToOne(targetEntity="InformationTeacher", cascade={"persist","remove"}, inversedBy="user")
     * @ORM\JoinColumn(name="informationTeacher", nullable=true)
     */
    private $informationTeacher;

    /**
     * @ORM\OneToOne(targetEntity="InformationAdministrator", cascade={"persist","remove"}, inversedBy="user")
     * @ORM\JoinColumn(name="informationAdministrator", nullable=true)
     */
    private $informationAdministrator;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Role", inversedBy="users")
     */
    private $function;

    public function __construct()
    {
        parent::__construct();
        $this->courses = new ArrayCollection();
        $this->courseCreated = new ArrayCollection();
    }

    public function getInformationStudent()
    {
        return $this->informationStudent;
    }

    public function setInformationStudent($informationStudent): ?self
    {
        $this->informationStudent = $informationStudent;
        return $this;
    }

    public function getInformationCompany()
    {
        return $this->informationCompany;
    }

    public function setInformationCompany($informationCompany): ?self
    {
        $this->informationCompany = $informationCompany;
        return $this;
    }

    public function getInformationTeacher()
    {
        return $this->informationTeacher;
    }

    public function setInformationTeacher($informationTeacher): ?self
    {
        $this->informationTeacher = $informationTeacher;
        return $this;
    }

    public function getInformationAdministrator()
    {
        return $this->informationAdministrator;
    }

    public function setInformationAdministrator($informationAdministrator): ?self
    {
        $this->informationAdministrator = $informationAdministrator;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFunction()
    {
        return $this->function;
    }

    public function setFunction($function): ?self
    {
        $this->function = $function;
        return $this;
    }

    public function getRoleName()
    {
        if ($this->function == null) {
            return "not defined";
        }
        return $this->function->getRoleName();
    }
}
