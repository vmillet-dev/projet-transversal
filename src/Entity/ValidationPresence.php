<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ValidationPresenceRepository")
 */
class ValidationPresence
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\InformationStudent", inversedBy="presence")
     */
    private $students;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Classes", inversedBy="presence")
     */
    private $classes;

    public function __construct()
    {
        //do nothing
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getStudents(): ?InformationStudent
    {
        return $this->students;
    }

    public function setStudents(?InformationStudent $students): self
    {
        $this->students = $students;
        return $this;
    }

    public function getClasses(): ?Classes
    {
        return $this->classes;
    }

    public function setClasses(?Classes $classes): self
    {
        $this->classes = $classes;
        return $this;
    }
}
