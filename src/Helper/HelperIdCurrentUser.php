<?php

namespace App\Helper;

class HelperIdCurrentUser
{
    public static function getIdUserCurrent($container)
    {
        return $container->get('security.token_storage')->getToken()->getUser()->getId();
    }
}
