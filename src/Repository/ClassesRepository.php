<?php

namespace App\Repository;

use App\Entity\Classes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Classes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Classes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Classes[]    findAll()
 * @method Classes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClassesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Classes::class);
    }

    public function addClasses(Classes $classes)
    {
        $this->_em->persist($classes);
        $this->_em->flush();
    }

    public function getAllClasses()
    {
        return $this->findAll();
    }

    public function getClassesByCreator($creator)
    {
        return $this->findBy(["creator" => $creator]);
    }

    public function getClassesWhereStudentIsPresent(int $idStudent)
    {
        return $this->createQueryBuilder('c')
            ->select('c')
            ->innerJoin('c.presence', 'p')
            ->innerJoin('p.students', 's')
            ->where('s.id = :idStudent')
            ->andWhere('c.endDate <= :now')
            ->setParameter('idStudent', $idStudent)
            ->setParameter('now', date("Y-m-d"))
            ->getQuery()
            ->getResult();
    }

    public function getClassesWhereStudentIsAbsent(int $idStudent)
    {
        $subQuery = $this->createQueryBuilder('q')
            ->select('q')
            ->innerJoin('q.presence', 'p')
            ->innerJoin('p.students', 's2')
            ->where('q = c')
            ->andWhere('s2.id = :idStudent');
        $qb = $this->createQueryBuilder('c')
            ->select('c')
            ->innerJoin('c.studentGroup', 'sg')
            ->innerJoin('sg.studdents', 's')
            ->where('s.id = :idStudent')
            ->andWhere('c.endDate <= :now')
            ->setParameter('now', date("Y-m-d"))
            ->setParameter('idStudent', $idStudent);
        $qb->andWhere($qb->expr()->not($qb->expr()->exists($subQuery->getDQL())));
        return $qb->getQuery()->getResult();
    }

    public function getClassesByStudentInFutur(int $idStudent)
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.studentGroup', 'sg')
            ->innerJoin('sg.studdents', 's')
            ->where('s.id = :idStudent')
            ->andWhere('c.beginDate > :now')
            ->setParameter('idStudent', $idStudent)
            ->setParameter('now', date("Y-m-d"))
            ->getQuery()
            ->getResult();
    }

    public function removeClasse(int $idClasses)
    {
        $classes = $this->find($idClasses);
        $this->_em->remove($classes);
        $this->_em->flush();
    }

    public function getClassesByStudentAndCode(int $idStudent, int $codeValue)
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.studentGroup', 'sg')
            ->innerJoin('sg.studdents', 's')
            ->innerJoin('c.QrCode', 'code')
            ->where('s.id = :idStudent')
            ->andWhere('code.value = :scannedCode')
            ->andWhere('c.endDate >= :now')
            ->setParameter('now', date("Y-m-d"))
            ->setParameter('idStudent', $idStudent)
            ->setParameter('scannedCode', $codeValue)
            ->getQuery()
            ->getResult();
    }
}
