<?php

namespace App\Repository;

use App\Entity\InformationAdministrator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class InformationAdministratorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InformationAdministrator::class);
    }

    public function removeInformation(int $idInformation)
    {
        $information = $this->find($idInformation);
        $this->_em->remove($information);
        $this->_em->flush();
    }
}
