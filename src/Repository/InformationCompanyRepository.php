<?php

namespace App\Repository;

use App\Entity\InformationCompany;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Classes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Classes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Classes[]    findAll()
 * @method Classes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InformationCompanyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InformationCompany::class);
    }

    public function getAllInformationCompany()
    {
        return $this->findAll();
    }
    
    public function removeInformation(int $idInformation)
    {
        $information = $this->find($idInformation);
        $this->_em->remove($information);
        $this->_em->flush();
    }
}
