<?php

namespace App\Repository;

use App\Entity\InformationStudent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Classes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Classes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Classes[]    findAll()
 * @method Classes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InformationStudentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InformationStudent::class);
    }

    public function getAllStudent()
    {
        return $this->createQueryBuilder("s")
            ->innerJoin("s.user", "u")
            ->where("u IS NOT null")
            ->getQuery()->getResult();
    }

    public function getCoursesByIdStud(int $idStudent)
    {
        return $this->find($idStudent)->getStudentGroup()->getCourses();
    }

    public function getInformationStudentById($idStudent)
    {
        return $this->find($idStudent);
    }

    public function removeInformation(int $idInformation)
    {
        $information = $this->find($idInformation);
        $this->_em->remove($information);
        $this->_em->flush();
    }
}
