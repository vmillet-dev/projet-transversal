<?php

namespace App\Repository;

use App\Entity\InformationTeacher;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method InformationTeacher|null find($id, $lockMode = null, $lockVersion = null)
 * @method InformationTeacher|null findOneBy(array $criteria, array $orderBy = null)
 * @method InformationTeacher[]    findAll()
 * @method InformationTeacher[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InformationTeacherRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InformationTeacher::class);
    }

    public function removeInformation(int $idInformation)
    {
        $information = $this->find($idInformation);
        $this->_em->remove($information);
        $this->_em->flush();
    }
}
