<?php

namespace App\Repository;

use App\Entity\Role;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class RoleRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Role::class);
    }

    public function isStudent(int $idRole): ?bool
    {
        return $this->find($idRole)->getRoleName() == "Student";
    }

    public function isSociety(int $idRole): ?bool
    {
        return $this->find($idRole)->getRoleName() == "InformationCompany";
    }

    public function isTeacher(int $idRole): ?bool
    {
        return $this->find($idRole)->getRoleName() == "Teacher";
    }

    public function isAdmin(int $idRole): ?bool
    {
        return $this->find($idRole)->getRoleName() == "Administrator";
    }

    public function getIdRole(string $role): ?int
    {
        $role = $this->findOneBy(["rolename" => $role]);
        return $role == null ? null : $role->getId();
    }
}
