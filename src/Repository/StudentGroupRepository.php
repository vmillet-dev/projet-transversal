<?php

namespace App\Repository;

use App\Entity\StudentGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method StudentGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method StudentGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method StudentGroup[]    findAll()
 * @method StudentGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StudentGroup::class);
    }

    public function addStudentGroup(StudentGroup $studentGroup)
    {
        $this->_em->persist($studentGroup);
        $this->_em->flush();
    }

    public function getAllStudentGroup()
    {
        return $this->findAll();
    }

    public function removeStudentGroup(int $idStudentGroup)
    {
        $studentGroup = $this->find($idStudentGroup);
        foreach ($studentGroup->getStuddents() as $student) {
            $student->setStudentGroup(null);
        }
        $this->_em->flush();
        $this->_em->remove($studentGroup);
        $this->_em->flush();
    }
}
