<?php

namespace App\Repository;

use App\Entity\InformationStudent;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Classes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Classes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Classes[]    findAll()
 * @method Classes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getRoleNameById(int $idUtilisateur): ?string
    {
        return $this->find($idUtilisateur)->getRole()->getRoleName();
    }

    public function getAllUser()
    {
        return $this->findAll();
    }

    public function getAllStudent()
    {
        $qb = $this->createQueryBuilder('u');
        $qb->where($qb->expr()->isNotNull("u.informationStudent"));

        return $qb->getQuery()
            ->getResult();
    }

    public function getStudentByCompany(int $idCompany)
    {
        return $this->createQueryBuilder("u")
            ->innerJoin("u.informationStudent", "s")
            ->innerJoin("s.company", "c")
            ->where("c.id = :idCompany")
            ->setParameter('idCompany', $idCompany)
            ->getQuery()->getResult();
    }

    public function updateInformationUser(User $user)
    {
        $this->_em->persist($user);
        $this->_em->flush();
    }
}
