<?php

namespace App\Repository;

use App\Entity\ValidationPresence;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Classes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Classes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Classes[]    findAll()
 * @method Classes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ValidationPresenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ValidationPresence::class);
    }

    public function addPresence($user, $course): ValidationPresence
    {
        $stillInBase = $this->createQueryBuilder("vp")
        ->select("vp")
        ->where("vp.students = :user")
        ->andWhere("vp.classes = :course")
        ->setParameter('user', $user)
        ->setParameter('course', $course)
        ->getQuery()->getResult();
        if (!$stillInBase) {
            $validation = new ValidationPresence();
            $validation->setStudents($user);
            $validation->setClasses($course);

            $this->_em->persist($validation);
            $this->_em->flush();
            return $validation;
        } else {
            return $stillInBase[0];
        }
    }
}
