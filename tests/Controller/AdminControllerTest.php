<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminControllerTest extends WebTestCase
{
    public function testVisitorAccess()
    {
        $client = static::createClient();

        // Try to access dashboard
        $client->request('GET', '/adminDashboard');
        // Ensure 302 "http:redirect" response from server.
        $this->assertTrue($client->getResponse()->isRedirect());
        // Ensure the response is redirecting to the login page.
        $this->assertRegExp('/\/login$/', $client->getResponse()->getTargetUrl());


        // Try to access user modification page
        $client->request('GET', '/modifyUser/1');
        // Ensure 302 "http:redirect" response from server.
        $this->assertTrue($client->getResponse()->isRedirect());
        // Ensure the response is redirecting to the login page.
        $this->assertRegExp('/\/login$/', $client->getResponse()->getTargetUrl());
    }

    public function testNoRoleAccess(){
        $client = static::createClient();

        // Log in as teacher
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username'  => 'norole1',
            '_password'  => 'norole',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access dashboard
        $client->request('GET', '/adminDashboard');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access user modification page
        $client->request('GET', '/modifyUser/1');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testTeacherAccess()
    {
        $client = static::createClient();

        // Log in as teacher
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username'  => 'teacher',
            '_password'  => 'teacher',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access dashboard
        $client->request('GET', '/adminDashboard');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access user modification page
        $client->request('GET', '/modifyUser/1');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testCompanyAccess()
    {
        $client = static::createClient();

        // Log in as company
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username'  => 'company',
            '_password'  => 'company',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access dashboard
        $client->request('GET', '/adminDashboard');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access user modification page
        $client->request('GET', '/modifyUser/1');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testStudentAccess()
    {
        $client = static::createClient();

        // Log in as student
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username'  => 'student',
            '_password'  => 'student',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access dashboard
        $client->request('GET', '/adminDashboard');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access user modification page
        $client->request('GET', '/modifyUser/1');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testAdminAccess()
    {
        $client = static::createClient();

        // Log in as admin
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username'  => 'admin',
            '_password'  => 'admin',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access dashboard
        $crawler = $client->request('GET', '/adminDashboard');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Ensure at least 1 ".presenceTable" in the response
        $this->assertGreaterThan(
            0,
            $crawler->filter('div[class="presenceTable"]')->count()
        );

        // Try to access user modification page
        $crawler = $client->request('GET', '/modifyUser/1');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Ensure 1 #remember_me input present on the page
        $this->assertEquals(
            1,
            $crawler->filter('select[id="form_function"]')->count()
        );
        // Ensure 1 #_submit button present on the page
        $this->assertEquals(
            1,
            $crawler->filter('button[id="form_save"]')->count()
        );
    }

    public function testAdminModifyInvalidUser()
    {
        $client = static::createClient();

        // Log in as admin
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username'  => 'admin',
            '_password'  => 'admin',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access user modification page with no user
        $client->request('GET', '/modifyUser');
        // Ensure 404 "http:not_found" response from server.
        $this->assertEquals(404, $client->getResponse()->getStatusCode());

        // Try to access user modification page with no corresponding user
        $client->request('GET', '/modifyUser/9999999');
        // Ensure 404 "http:not_found" response from server.
        $this->assertEquals(404, $client->getResponse()->getStatusCode());

        // Try to access user modification page on user 1
        $crawler = $client->request('GET', '/modifyUser/1');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Ensure 1 form named form in the response
        $this->assertEquals(
            1,
            $crawler->filter('form[name="form"]')->count()
        );
    }

    public function testAdminModifyUser(){
        $client = static::createClient();

        // Log in as admin
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username'  => 'admin',
            '_password'  => 'admin',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access "norole2" user modification
        $crawler = $client->request('GET', '/modifyUser/22');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Extract a form object
        $form = $crawler->selectButton("Save")->form();
        // Select function with value = 1 (company)
        $form["form[function]"]->select(1);
        // Set the necessary company information
        $form["form[informationCompany][name]"]->setValue("aNewCompany");
        // Send the post request
        $client->submit($form);
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Reload the page
        $crawler = $client->request('GET', '/modifyUser/22');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Extract a form object
        $form = $crawler->selectButton("Save")->form();
        // Ensure function value is 1 (company)
        $this->assertEquals(1, $form["form[function]"]->getValue());
        // Ensure company name is "aNewCompany"
        $this->assertEquals("aNewCompany", $form["form[informationCompany][name]"]->getValue());

        // Try to access "norole3" user modification
        $crawler = $client->request('GET', '/modifyUser/23');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Extract a form object
        $form = $crawler->selectButton("Save")->form();
        // Select function with value = 2 (student)
        $form["form[function]"]->select(2);
        // Select company with value = 1 (Tecknet Inc(non-default))
        $form["form[informationStudent][company]"]->select(1);
        // Set the startDateContrat year to 2015
        $form["form[informationStudent][startDateContrat][year]"]->select(2015);
        // Set the startDateContrat month to 2 (feb)
        $form["form[informationStudent][startDateContrat][month]"]->select(2);
        // Set the startDateContrat day to 3
        $form["form[informationStudent][startDateContrat][day]"]->select(3);
        // Set the endDateContrat year to 2015
        $form["form[informationStudent][endDateContrat][year]"]->select(2016);
        // Set the endDateContrat month to 4 (apr)
        $form["form[informationStudent][endDateContrat][month]"]->select(4);
        // Set the endDateContrat day to 5
        $form["form[informationStudent][endDateContrat][day]"]->select(5);
        // Send the post request
        $client->submit($form);
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Reload the page
        $crawler = $client->request('GET', '/modifyUser/23');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Extract a form object
        $form = $crawler->selectButton("Save")->form();
        // Ensure function value is 2 (student)
        $this->assertEquals(2, $form["form[function]"]->getValue());
        // Ensure company have value = 1 (Tecknet Inc(non-default))
        $this->assertEquals(1, $form["form[informationStudent][company]"]->getValue());
        // Ensure startDateContrat year have value = 2015
        $this->assertEquals(2015, $form["form[informationStudent][startDateContrat][year]"]->getValue());
        // Ensure startDateContrat month have value = 2 (feb)
        $this->assertEquals(2, $form["form[informationStudent][startDateContrat][month]"]->getValue());
        // Ensure startDateContrat day have value = 3
        $this->assertEquals(3, $form["form[informationStudent][startDateContrat][day]"]->getValue());
        // Ensure endDateContrat year have value = 2016
        $this->assertEquals(2016, $form["form[informationStudent][endDateContrat][year]"]->getValue());
        // Ensure endDateContrat month have value = 4 (apr)
        $this->assertEquals(4, $form["form[informationStudent][endDateContrat][month]"]->getValue());
        // Ensure endDateContrat day have value = 5
        $this->assertEquals(5, $form["form[informationStudent][endDateContrat][day]"]->getValue());

        // Try to access "norole4" user modification
        $crawler = $client->request('GET', '/modifyUser/24');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Extract a form object
        $form = $crawler->selectButton("Save")->form();
        // Select function with value = 3 (teacher)
        $form["form[function]"]->select(3);
        // Send the post request
        $client->submit($form);
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Reload the page
        $crawler = $client->request('GET', '/modifyUser/24');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Extract a form object
        $form = $crawler->selectButton("Save")->form();
        // Ensure function value is 3 (teacher)
        $this->assertEquals(3, $form["form[function]"]->getValue());

        // Try to access "norole5" user modification
        $crawler = $client->request('GET', '/modifyUser/25');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Extract a form object
        $form = $crawler->selectButton("Save")->form();
        // Select function with value = 4 (admin)
        $form["form[function]"]->select(4);
        // Send the post request
        $client->submit($form);
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Reload the page
        $crawler = $client->request('GET', '/modifyUser/25');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Extract a form object
        $form = $crawler->selectButton("Save")->form();
        // Ensure function value is 4 (admin) <- bad test as 4 is the default value
        $this->assertEquals(4, $form["form[function]"]->getValue());
    }
}