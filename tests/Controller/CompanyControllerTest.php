<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CompanyControllerTest extends WebTestCase
{
    public function testVisitorAccess()
    {
        $client = static::createClient();

        // Try to access student list
        $client->request('GET', '/studentList');
        // Ensure 302 "http:redirect" response from server.
        $this->assertTrue($client->getResponse()->isRedirect());
        // Ensure the response is redirecting to the login page.
        $this->assertRegExp('/\/login$/', $client->getResponse()->getTargetUrl());

        // Try to access student courses
        $client->request('GET', '/studentCourses/1');
        // Ensure 302 "http:redirect" response from server.
        $this->assertTrue($client->getResponse()->isRedirect());
        // Ensure the response is redirecting to the login page.
        $this->assertRegExp('/\/login$/', $client->getResponse()->getTargetUrl());

    }

    public function testTeacherAccess()
    {
        $client = static::createClient();

        // Log in as teacher
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username'  => 'teacher',
            '_password'  => 'teacher',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access student list
        $client->request('GET', '/studentList');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access the student courses
        $client->request('GET', '/studentCourses/1');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testStudentAccess()
    {
        $client = static::createClient();

        // Log in as student
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => 'student',
            '_password' => 'student',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access student list
        $client->request('GET', '/studentList');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access other student courses
        $client->request('GET', '/studentCourses/2');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access his own student courses(via the company page)
        $client->request('GET', '/studentCourses/2');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testAdminAccess()
    {
        $client = static::createClient();

        // Log in as admin
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => 'admin',
            '_password' => 'admin',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access the student list
        $client->request('GET', '/studentList');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access student courses
        $client->request('GET', '/studentCourses/1');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testCompanyAccess()
    {
        $client = static::createClient();

        // Log in as company
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => 'company',
            '_password' => 'company',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access the student list
        $crawler = $client->request('GET', '/studentList');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Ensure 1 ".presenceTable" in the response
        $this->assertEquals(
            1,
            $crawler->filter('div[class="presenceTable"]')->count()
        );

        // Try to access the student list of his student
        $crawler = $client->request('GET', '/studentCourses/1');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Ensure 1 ".presenceTable" in the response
        $this->assertEquals(
            1,
            $crawler->filter('div[class="presenceTable"]')->count()
        );

        // Try to access the student list of another student
        $crawler = $client->request('GET', '/studentCourses/2');
        // Ensure 200 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

    }
}
