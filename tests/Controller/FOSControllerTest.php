<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FOSControllerTest extends WebTestCase
{
    public function testShowLogin()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/login');

        // Ensure 200 "http:OK" response from server
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // Ensure 1 #username input present on the page
        $this->assertEquals(
            1,
            $crawler->filter('input[id="username"]')->count()
        );
        // Ensure 1 #password input present on the page
        $this->assertEquals(
            1,
            $crawler->filter('input[id="password"]')->count()
        );
        // Ensure 1 #remember_me input present on the page
        $this->assertEquals(
            1,
            $crawler->filter('input[id="remember_me"]')->count()
        );
        // Ensure 1 link to register present on the page
        $this->assertEquals(
            1,
            $crawler->selectLink('Create account')->count()
        );
        // Ensure 1 #_submit button present on the page
        $this->assertEquals(
            1,
            $crawler->filter('button[id="_submit"]')->count()
        );
    }
    public function testShowRegister()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/register/');

        // Ensure 200 "http:OK" response from server
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // Ensure 1 #fos_user_registration_form_email input present on the page
        $this->assertEquals(
            1,
            $crawler->filter('input[id="fos_user_registration_form_email"]')->count()
        );

        // Ensure 1 #fos_user_registration_form_username input present on the page
        $this->assertEquals(
            1,
            $crawler->filter('input[id="fos_user_registration_form_username"]')->count()
        );

        // Ensure 1 #fos_user_registration_form_plainPassword_first input present on the page
        $this->assertEquals(
            1,
            $crawler->filter('input[id="fos_user_registration_form_plainPassword_first"]')->count()
        );

        // Ensure 1 #fos_user_registration_form_plainPassword_second input present on the page
        $this->assertEquals(
            1,
            $crawler->filter('input[id="fos_user_registration_form_plainPassword_second"]')->count()
        );

        // Ensure 1 submit input present on the page
        $this->assertEquals(
            1,
            $crawler->filter('input[type="submit"]')->count()
        );
    }
}