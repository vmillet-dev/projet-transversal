<?php

namespace App\Tests\Form;

use App\Entity\User;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class LogFormTest extends WebTestCase
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        static::bootKernel();
        $this->entityManager = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->entityManager->getConnection()->beginTransaction(); // suspend auto-commit
        $this->entityManager->getConnection()->setAutoCommit(false);
        parent::__construct($name, $data, $dataName);
    }

    public function testSignIn()
    {
        $client = static::createClient();

        $hash = md5(time());

        $defaultEncoder = new MessageDigestPasswordEncoder('sha512', true, 5000);
        $encoders = [
            User::class => $defaultEncoder,
        ];

        $user = new User();
        $user->setPassword((new UserPasswordEncoder(new EncoderFactory($encoders)))->encodePassword($user, $hash));
        $user->setUsername($hash);
        $user->setUsernameCanonical($hash);
        $user->setEnabled(true);
        $user->setEmail($hash."@tecknet.com");
        $user->setEmailCanonical($hash."@tecknet.com");

        try {
            $this->entityManager->persist($user);
            $this->entityManager->getConnection()->commit();
            $this->entityManager->flush();
        } catch (ORMException $e) {
        } catch (ConnectionException $e) {
        }

        // Log in as generated string
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => $hash,
            '_password' => $hash,
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        try {
            $this->entityManager->getConnection()->beginTransaction(); // suspend auto-commit
            $this->entityManager->remove($user);
            $this->entityManager->getConnection()->commit();
            $this->entityManager->flush();
        } catch (ORMException $e) {
        } catch (ConnectionException $e) {
        }
    }

    public function testSignUp()
    {
        $client = static::createClient();

        $hash = md5(time());

        // Sign up with specifique value
        $client->followRedirects(true);
        $crawler = $client->request('GET', '/register');
        $client->followRedirects(false);
        $form = $crawler->selectButton('_submit')->form(array(
            'fos_user_registration_form[email]' => $hash.'@univ.fr',
            'fos_user_registration_form[username]' => $hash,
            'fos_user_registration_form[plainPassword][first]' => $hash,
            'fos_user_registration_form[plainPassword][second]' => $hash,
        ));
        $client->submit($form);

        //Check if the request do the job
        $crawlerCheck = $client->request('GET', '/login');
        $formCheck = $crawlerCheck->selectButton('_submit')->form(array(
            '_username'  => $hash,
            '_password'  => $hash,
        ));
        $client->submit($formCheck);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect()); //Ne test pas si la personne a reussi à se connecter
    }
}