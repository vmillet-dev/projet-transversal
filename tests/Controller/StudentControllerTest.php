<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class StudentControllerTest extends WebTestCase
{
    public function testVisitorAccess()
    {
        $client = static::createClient();

        // Try to access the course scan page
        $client->request('GET', '/scanCode');
        // Ensure 302 "http:redirect" response from server.
        $this->assertTrue($client->getResponse()->isRedirect());
        // Ensure the response is redirecting to the login page.
        $this->assertRegExp('/\/login$/', $client->getResponse()->getTargetUrl());

        // Try to access the Qr validation page
        $client->request('GET', '/verifCode/333333');
        // Ensure 302 "http:redirect" response from server.
        $this->assertTrue($client->getResponse()->isRedirect());
        // Ensure the response is redirecting to the login page.
        $this->assertRegExp('/\/login$/', $client->getResponse()->getTargetUrl());

        // Try to access the course list page
        $client->request('GET', '/courses');
        // Ensure 302 "http:redirect" response from server.
        $this->assertTrue($client->getResponse()->isRedirect());
        // Ensure the response is redirecting to the login page.
        $this->assertRegExp('/\/login$/', $client->getResponse()->getTargetUrl());
    }

    public function testNoRoleAccess(){
        $client = static::createClient();

        // Log in as teacher
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username'  => 'norole1',
            '_password'  => 'norole',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access the course scan page
        $client->request('GET', '/scanCode');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access the Qr validation page
        $client->request('GET', '/verifCode/333333');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access the course list page
        $client->request('GET', '/courses');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testTeacherAccess()
    {
        $client = static::createClient();

        // Log in as teacher
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username'  => 'teacher',
            '_password'  => 'teacher',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access the course scan page
        $client->request('GET', '/scanCode');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access the Qr validation page
        $client->request('GET', '/verifCode/333333');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access the course list page
        $client->request('GET', '/courses');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testCompanyAccess()
    {
        $client = static::createClient();

        // Log in as company
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => 'company',
            '_password' => 'company',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access the course scan page
        $client->request('GET', '/scanCode');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access the Qr validation page
        $client->request('GET', '/verifCode/333333');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access the course list page
        $client->request('GET', '/courses');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testAdminAccess()
    {
        $client = static::createClient();

        // Log in as admin
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username'  => 'admin',
            '_password'  => 'admin',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access the course scan page
        $client->request('GET', '/scanCode');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access the Qr validation page
        $client->request('GET', '/verifCode/333333');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access the course list page
        $client->request('GET', '/courses');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testStudentAccess()
    {
        $client = static::createClient();

        // Log in as student
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username'  => 'student',
            '_password'  => 'student',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access the course scan page
        $crawler = $client->request('GET', '/scanCode');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Ensure at least 1 ".content container" in the response
        $this->assertGreaterThan(
            0,
            $crawler->filter('div[class="content container"]')->count()
        );
        // Try to access the Qr validation page (with wrong code)
        $client->request('GET', '/verifCode/666666');
        // Ensure 404 "http:not found" response from server.
        $this->assertEquals(404, $client->getResponse()->getStatusCode());

        // Try to access the Qr validation page (with expired code)
        $client->request('GET', '/verifCode/555555');
        // Ensure 404 "http:not found" response from server.
        $this->assertEquals(404, $client->getResponse()->getStatusCode());

        // Try to access the Qr validation page
        $client->request('GET', '/verifCode/333333');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // Try to access the course list page
        $crawler = $client->request('GET', '/courses');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Ensure 1 ".presenceTable" in the response
        $this->assertEquals(
            1,
            $crawler->filter('div[class="presenceTable"]')->count()
        );
    }
}