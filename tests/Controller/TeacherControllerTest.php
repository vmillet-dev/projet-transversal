<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TeacherControllerTest extends WebTestCase
{
    public function testVisitorAccess()
    {
        $client = static::createClient();

        // Try to access class creation
        $client->request('GET', '/createClass');
        // Ensure 302 "http:redirect" response from server.
        $this->assertTrue($client->getResponse()->isRedirect());
        // Ensure the response is redirecting to the login page.
        $this->assertRegExp('/\/login$/', $client->getResponse()->getTargetUrl());


        // Try to access class list
        $client->request('GET', '/listClass');
        // Ensure 302 "http:redirect" response from server.
        $this->assertTrue($client->getResponse()->isRedirect());
        // Ensure the response is redirecting to the login page.
        $this->assertRegExp('/\/login$/', $client->getResponse()->getTargetUrl());


        // Try to access student group deletion
        $client->request('GET', '/deleteStudentGroup/2');
        // Ensure 302 "http:redirect" response from server.
        $this->assertTrue($client->getResponse()->isRedirect());
        // Ensure the response is redirecting to the login page.
        $this->assertRegExp('/\/login$/', $client->getResponse()->getTargetUrl());


        // Try to access course creation
        $client->request('GET', '/createCourses');
        // Ensure 302 "http:redirect" response from server.
        $this->assertTrue($client->getResponse()->isRedirect());
        // Ensure the response is redirecting to the login page.
        $this->assertRegExp('/\/login$/', $client->getResponse()->getTargetUrl());

        // Try to access course list
        $client->request('GET', '/listCourses');
        // Ensure 302 "http:redirect" response from server.
        $this->assertTrue($client->getResponse()->isRedirect());
        // Ensure the response is redirecting to the login page.
        $this->assertRegExp('/\/login$/', $client->getResponse()->getTargetUrl());

        // Try to access course deletion
        $client->request('GET', '/deleteCourses/2');
        // Ensure 302 "http:redirect" response from server.
        $this->assertTrue($client->getResponse()->isRedirect());
        // Ensure the response is redirecting to the login page.
        $this->assertRegExp('/\/login$/', $client->getResponse()->getTargetUrl());

        // Try to access qr generation
        $client->request('GET', '/displayQrCode/9999');
        // Ensure 302 "http:redirect" response from server.
        $this->assertTrue($client->getResponse()->isRedirect());
        // Ensure the response is redirecting to the login page.
        $this->assertRegExp('/\/login$/', $client->getResponse()->getTargetUrl());
    }

    public function testStudentAccess()
    {
        $client = static::createClient();

        // Log in as student
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => 'student',
            '_password' => 'student',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());


        // Try to access class creation
        $client->request('GET', '/createClass');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access class list
        $client->request('GET', '/listClass');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access student group deletion
        $client->request('GET', '/deleteStudentGroup/2');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access course creation
        $client->request('GET', '/createCourses');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access course list
        $client->request('GET', '/listCourses');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access course deletion
        $client->request('GET', '/deleteCourses/2');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access qr generation
        $client->request('GET', '/displayQrCode/9999');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testCompanyAccess()
    {
        $client = static::createClient();

        // Log in as company
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => 'company',
            '_password' => 'company',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());


        // Try to access class creation
        $client->request('GET', '/createClass');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access class list
        $client->request('GET', '/listClass');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access student group deletion
        $client->request('GET', '/deleteStudentGroup/2');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access course creation
        $client->request('GET', '/createCourses');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access course list
        $client->request('GET', '/listCourses');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access course deletion
        $client->request('GET', '/deleteCourses/2');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access qr generation
        $client->request('GET', '/displayQrCode/9999');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testAdminAccess()
    {
        $client = static::createClient();

        // Log in as admin
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => 'admin',
            '_password' => 'admin',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());


        // Try to access class creation
        $client->request('GET', '/createClass');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access class list
        $client->request('GET', '/listClass');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access student group deletion
        $client->request('GET', '/deleteStudentGroup/2');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access course creation
        $client->request('GET', '/createCourses');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access course list
        $client->request('GET', '/listCourses');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access course deletion
        $client->request('GET', '/deleteCourses/2');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        // Try to access qr generation
        $client->request('GET', '/displayQrCode/9999');
        // Ensure 403 "http:forbidden" response from server.
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testTeacherClassAccess()
    {
        $client = static::createClient();

        // Log in as teacher
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => 'teacher',
            '_password' => 'teacher',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());


        // Try to access class creation
        $crawler = $client->request('GET', '/createClass');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Ensure 1 #formClassList form present on the page
        $this->assertEquals(
            1,
            $crawler->filter('form[id="formClassList"]')->count()
        );
        // Ensure 1 #nameClasses input present on the page
        $this->assertEquals(
            1,
            $crawler->filter('input[id="nameClasses"]')->count()
        );
        // Ensure 1 #bodyStudentList tbody present on the page
        $this->assertEquals(
            1,
            $crawler->filter('tbody[id="bodyStudentList"]')->count()
        );
        // Ensure 1 #bodyClassList tbody present on the page
        $this->assertEquals(
            1,
            $crawler->filter('tbody[id="bodyClassList"]')->count()
        );

        // Try to access class list
        $crawler = $client->request('GET', '/listClass');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Ensure at least 1 ".presenceTable" in the response
        $this->assertGreaterThan(
            0,
            $crawler->filter('div[class="presenceTable"]')->count()
        );

        // Try to access class deletion  (with no id param)
        $client->request('GET', '/deleteStudentGroup');
        // Ensure 404 "http:not found" response from server.
        $this->assertEquals(404, $client->getResponse()->getStatusCode());


        // Try to access student group deletion  (with invalid id param)
        $client->request('GET', '/deleteStudentGroup/9999');
        // Ensure 404 "http:not found" response from server.
        $this->assertEquals(404, $client->getResponse()->getStatusCode());

        // Try to access student group deletion
        $client->request('GET', '/deleteStudentGroup/2');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testTeacherCoursesAccess(){
        $client = static::createClient();

        // Log in as teacher
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => 'teacher',
            '_password' => 'teacher',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access course creation
        $crawler = $client->request('GET', '/createCourses');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Ensure 1 #formClassList form present on the page
        $this->assertEquals(
            1,
            $crawler->filter('form[id="formClassList"]')->count()
        );
        // Ensure 1 #nameClasses input present on the page
        $this->assertEquals(
            1,
            $crawler->filter('input[id="nameCourses"]')->count()
        );
        // Ensure 1 input named "beginDateCourses" present on the page
        $this->assertEquals(
            1,
            $crawler->filter('input[name="beginDateCourses"]')->count()
        );
        // Ensure 1 input named "endDateCourses" present on the page
        $this->assertEquals(
            1,
            $crawler->filter('input[name="endDateCourses"]')->count()
        );
        // Ensure 1 #bodyClassList tbody present on the page
        $this->assertEquals(
            1,
            $crawler->filter('tbody[id="bodyClassList"]')->count()
        );
        // Ensure 1 #bodyCoursesList tbody present on the page
        $this->assertEquals(
            1,
            $crawler->filter('tbody[id="bodyCoursesList"]')->count()
        );


        // Try to access course list
        $crawler = $client->request('GET', '/listCourses');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Ensure at least 1 ".presenceTable" in the response
        $this->assertGreaterThan(
            0,
            $crawler->filter('div[class="presenceTable"]')->count()
        );

        // Try to access course deletion (with no id param)
        $client->request('GET', '/deleteCourses');
        // Ensure 404 "http:not found" response from server.
        $this->assertEquals(404, $client->getResponse()->getStatusCode());

        // Try to access course deletion (with invalid id param)
        $client->request('GET', '/deleteCourses/99999');
        // Ensure 404 "http:not found" response from server.
        $this->assertEquals(404, $client->getResponse()->getStatusCode());

        // Try to access course deletion
        $client->request('GET', '/deleteCourses/2');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testTeacherQrAccess()
    {
        $client = static::createClient();

        // Log in as teacher
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => 'teacher',
            '_password' => 'teacher',
        ));
        $client->submit($form);
        // Ensure login worked
        $this->assertTrue($client->getResponse()->isRedirect());

        // Try to access qr image generation (with no code param)
        $client->request('GET', '/displayQrCode');
        // Ensure 404 "http:not found" response from server.
        $this->assertEquals(404, $client->getResponse()->getStatusCode());

        // Try to access qr image generation
        $crawler = $client->request('GET', '/displayQrCode/9999');
        // Ensure 200 "http:ok" response from server.
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Ensure at 1 ".container > img" in the response
        $this->assertEquals(
            1,
            $crawler->filter('div[class="container"]')->filter('img')->count()
        );
    }
}
