<?php

namespace App\Tests\Repository;

use App\Entity\Classes;
use App\Entity\QrCode;
use App\Entity\StudentGroup;
use DateTime;
use Proxies\__CG__\App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ClassesRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    public function init()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->entityManager->flush();
    }

    public function end()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }


    public function testRemoveClasses()
    {
        $this->init();

        $repository = $this->entityManager
            ->getRepository(Classes::class);

        $classe = $repository->findOneBy(["nameUe" => "M1INF01 - PHP"]);
        $this->assertNotEquals(null, $classe);
        $idClasse = $classe->getId();
        $repository->removeClasse($idClasse);

        $classe = $repository->find($idClasse);
        $this->assertEquals(null, $classe);

        $this->end();
    }

    public function testAddClasses()
    {
        $this->init();

        $repository = $this->entityManager
            ->getRepository(StudentGroup::class);

        $studentGroup = $repository->findOneBy(["groupName" => "Group A"]);
        $this->assertNotEquals(null, $studentGroup);

        $classe = new Classes();
        $classe->setBeginDate(new DateTime());
        $classe->setEndDate(new DateTime());
        $classe->setNameUe("nameUeTest");
        $classe->setStudentGroup($studentGroup);

        $repository = $this->entityManager
            ->getRepository(Classes::class);

        $repository->addClasses($classe);
        $idClasse = $classe->getId();

        $result = $repository->find($idClasse);
        $this->assertNotEquals(null, $result);

        $this->end();
    }

    public function testGetAllClasses()
    {
        $this->init();

        $repository = $this->entityManager
            ->getRepository(Classes::class);
        $result = $repository->getAllClasses();
        $this->assertNotEquals(null, $result);
        $this->assertEquals(true, is_array($result));

        $this->end();
    }

    public function testGetClassesByCreator()
    {
        $this->init();

        $repository = $this->entityManager
            ->getRepository(User::class);
        $user = $repository->findOneBy(["username" => "teacher"]);
        $this->assertNotEquals(null, $user);

        $repository = $this->entityManager
            ->getRepository(Classes::class);
        $result = $repository->getClassesByCreator($user);
        $this->assertNotEquals(null, $result);
        $this->assertEquals(true, is_array($result));

        $this->end();
    }

    public function testGetClassesWhereStudentIsPresent()
    {
        $this->init();

        $repository = $this->entityManager
            ->getRepository(User::class);
        $user = $repository->findOneBy(["username" => "student"]);
        $this->assertNotEquals(null, $user);

        $informationStudent = $user->getInformationStudent();
        $this->assertNotEquals(null, $informationStudent);

        $idStudent = $informationStudent->getId();

        $repository = $this->entityManager
            ->getRepository(Classes::class);
        $result = $repository->getClassesWhereStudentIsPresent($idStudent);
        $this->assertNotEquals(null, $result);
        $this->assertEquals(true, is_array($result));

        $this->end();
    }

    public function testGetClassesWhereStudentIsAbsent()
    {
        $this->init();

        $repository = $this->entityManager
            ->getRepository(User::class);
        $user = $repository->findOneBy(["username" => "student"]);
        $this->assertNotEquals(null, $user);

        $informationStudent = $user->getInformationStudent();
        $this->assertNotEquals(null, $informationStudent);

        $idStudent = $informationStudent->getId();

        $repository = $this->entityManager
            ->getRepository(Classes::class);
        $result = $repository->getClassesWhereStudentIsAbsent($idStudent);
        $this->assertNotEquals(null, $result);
        $this->assertEquals(true, is_array($result));

        $this->end();
    }

    public function testGetClassesByStudentInFutur()
    {
        $this->init();

        $repository = $this->entityManager
            ->getRepository(User::class);
        $user = $repository->findOneBy(["username" => "student"]);
        $this->assertNotEquals(null, $user);

        $informationStudent = $user->getInformationStudent();
        $this->assertNotEquals(null, $informationStudent);

        $idStudent = $informationStudent->getId();

        $repository = $this->entityManager
            ->getRepository(Classes::class);
        $result = $repository->getClassesByStudentInFutur($idStudent);
        $this->assertNotEquals(null, $result);
        $this->assertEquals(true, is_array($result));

        $this->end();
    }

    // public function testGetClassesByStudentAndCode()
    // {
    //     $this->init();

    //     $repository = $this->entityManager
    //         ->getRepository(QrCode::class);
    //     $qrCode = $repository->findOneBy(["value" => "621896"]);
    //     $this->assertNotEquals(null, $qrCode);

    //     $repository = $this->entityManager
    //         ->getRepository(User::class);
    //     $user = $repository->findOneBy(["username" => "student"]);
    //     $this->assertNotEquals(null, $user);

    //     $repository = $this->entityManager
    //         ->getRepository(Classes::class);
    //     $idStudent = $user->getInformationStudent()->getId();
    //     $codeValue = $qrCode->getValue();
    //     $result = $repository->getClassesByStudentAndCode($idStudent, $codeValue);
    //     $this->assertNotEquals(null, $result);
    //     $this->assertEquals(true, is_array($result));
    //     $this->assertEquals(1, count($result));

    //     $this->end();
    // }
}
