<?php

namespace App\Tests\Repository;

use App\Entity\InformationCompany;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class InformationCompanyRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    public function init()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function end()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testRemoveInformation()
    {

        $this->init();

        $newInformationCompany = new InformationCompany();
        $newInformationCompany->setName("test");
        $repository = $this->entityManager
            ->getRepository(InformationCompany::class);

        $this->entityManager->persist($newInformationCompany);
        $this->entityManager->flush();
        $id = $newInformationCompany->getId();
        $repository->removeInformation($id);

        $information = $repository->find($id);

        $this->assertEquals(null, $information);

        $this->end();
    }
}
