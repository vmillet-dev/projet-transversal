<?php

namespace App\Tests\Repository;

use App\Entity\InformationStudent;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class InformationStudentRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    public function init()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function end()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testRemoveInformation()
    {
        $this->init();

        $newInformationStudent = new InformationStudent();
        $repository = $this->entityManager
            ->getRepository(InformationStudent::class);

        $this->entityManager->persist($newInformationStudent);
        $this->entityManager->flush();
        $id = $newInformationStudent->getId();
        $repository->removeInformation($id);

        $information = $repository->find($id);

        $this->assertEquals(null, $information);

        $this->end();
    }
}