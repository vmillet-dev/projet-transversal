<?php

namespace App\Tests\Repository;

use App\Entity\InformationTeacher;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class InformationTeacherRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    public function init()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function end()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testRemoveInformation()
    {
        $this->init();

        $newInformationTeacher = new InformationTeacher();
        $repository = $this->entityManager
            ->getRepository(InformationTeacher::class);

        $this->entityManager->persist($newInformationTeacher);
        $this->entityManager->flush();
        $id = $newInformationTeacher->getId();
        $repository->removeInformation($id);

        $information = $repository->find($id);

        $this->assertEquals(null, $information);

        $this->end();
    }
}
