<?php

namespace App\Tests\Repository;

use App\Entity\QrCode;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class QrCodeRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    public function init()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function end()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testAddQrCode()
    {

        $this->init();

        $value = 1234567;
        $repository = $this->entityManager
            ->getRepository(QrCode::class);
        $qrCode = $repository->addQrCode($value);
        $id = $qrCode->getId();

        $result = $repository->find($id);


        $this->assertNotEquals(null, $result);
        $this->assertEquals($value,$result->getValue());

        $this->end();
    }
}
