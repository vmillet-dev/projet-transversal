<?php

namespace App\Tests\Repository;

use App\Entity\Role;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RoleRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    public function init()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->entityManager->flush();
    }

    public function end()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testGetIdRole()
    {
        $this->init();

        $repository = $this->entityManager
            ->getRepository(Role::class);

        $idAdmin = $repository->getIdRole("Administrator");
        $this->assertNotEquals(null,$idAdmin);

        $idCompany = $repository->getIdRole("Company");
        $this->assertNotEquals(null,$idCompany);

        $idStudent = $repository->getIdRole("Student");
        $this->assertNotEquals(null,$idStudent);

        $idTeacher = $repository->getIdRole("Teacher");
        $this->assertNotEquals(null,$idTeacher);

        $idNull = $repository->getIdRole("thisTestIsNull");
        $this->assertEquals(null,$idNull);

        $this->end();
    }
}
