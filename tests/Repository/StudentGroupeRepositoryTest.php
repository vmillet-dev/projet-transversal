<?php

namespace App\Tests\Repository;

use App\Entity\User;
use App\Entity\StudentGroup;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class StudentGroupRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    public function init()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->entityManager->flush();
    }

    public function end()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testAddStudentGroup()
    {
        $this->init();

        $studentGroup = new StudentGroup();
        $studentGroup->setGroupName("test");
        $this->entityManager->persist($studentGroup);
        $this->entityManager->flush();

        $repository = $this->entityManager
            ->getRepository(StudentGroup::class);

        $id = $studentGroup->getId();
        $result = $repository->find($id);

        $this->assertNotEquals(null, $result);

        $this->end();
    }

    public function testGetAllStudentGroup()
    {
        $this->init();

        $repository = $this->entityManager
            ->getRepository(StudentGroup::class);

        $result = $repository->getAllStudentGroup();
        $this->assertNotEquals(null, $result);
        $this->assertEquals(true, is_array($result));

        $this->end();
    }

    public function testGetRemoveStudentGroup()
    {
        $this->init();

        $repository = $this->entityManager
            ->getRepository(User::class);
        $teacher = $repository->findOneBy(["username" => "teacher"]);
        $this->assertNotEquals(null, $teacher);

        $studentGroup = $teacher->getInformationTeacher()->getStudentGroups()[0];
        $this->assertNotEquals(null, $studentGroup);

        $listStudent = $studentGroup->getStuddents();
        $idStudentGroup = $studentGroup->getId();

        $repository = $this->entityManager
            ->getRepository(StudentGroup::class);
        $repository->removeStudentGroup($idStudentGroup);
        $result = $repository->find($idStudentGroup);
        $this->assertEquals(null, $result);

        $isNotInGroup = true;

        foreach ($listStudent as $student) {
            if ($student->getStudentGroup() != null) {
                $isNotInGroup = false;
                break;
            }
        }

        $this->assertEquals(true, $isNotInGroup);

        $this->end();
    }
}
