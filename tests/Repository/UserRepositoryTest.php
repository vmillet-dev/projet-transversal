<?php

namespace App\Tests\Repository;

use App\Entity\InformationCompany;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    public function init()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->entityManager->flush();
    }

    public function end()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testGetAllUser()
    {
        $this->init();

        $repository = $this->entityManager
            ->getRepository(User::class);

        $result = $repository->getAllUser();

        $this->assertNotEquals(null, $result);
        $this->assertEquals(true, is_array($result));

        $this->end();
    }

    public function testGetAllStudent()
    {
        $this->init();

        $repository = $this->entityManager
            ->getRepository(User::class);

        $result = $repository->getAllStudent();

        $this->assertNotEquals(null, $result);
        $this->assertEquals(true, is_array($result));

        $isAllStudent = true;

        foreach ($result as $student) {
            if ($student->getInformationStudent() == null) {
                $isAllStudent = false;
            }
        }

        $this->assertEquals(true, $isAllStudent);

        $this->end();
    }

    public function testGetStudentByCompany()
    {
        $this->init();

        $repository = $this->entityManager
            ->getRepository(InformationCompany::class);
        $informationCompany = $repository->findOneBy(["name" => "Tecknet Inc."]);
        $this->assertNotEquals(null, $informationCompany);

        $repository = $this->entityManager
            ->getRepository(User::class);

        $idInfomartionCompany = $informationCompany->getId();
        $result = $repository->getStudentByCompany($idInfomartionCompany);
        $this->assertNotEquals(null, $result);
        $this->assertEquals(true, is_array($result));

        $isInCompany = true;

        foreach ($result as $userStudent) {
            $idCompany = $userStudent->getInformationStudent()
                ->getCompany()->getId();
            if ($idInfomartionCompany != $idCompany) {
                $isInCompany = false;
            }
        }

        $this->assertEquals(true, $isInCompany);

        $this->end();
    }

    public function testUpdateInformationUser()
    {
        $this->init();

        $user = new User();
        $user->setUsername("testTestTest");
        $user->setPassword("testTestTest");
        $user->setEmail("testTestTest");

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $repository = $this->entityManager
            ->getRepository(User::class);

        $result = $repository->find($user->getId());
        $this->assertNotEquals(null, $result);

        $username = $result->getUsername();
        $result->setUsername("otherUsernameTest");
        $repository->UpdateInformationUser($result);

        $result = $repository->find($result->getId());
        $this->assertNotEquals(null, $result);
        $this->assertNotEquals($username, $result->getUsername());

        $this->end();
    }
}
