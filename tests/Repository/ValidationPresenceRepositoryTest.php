<?php

namespace App\Tests\Repository;

use App\Entity\User;
use App\Entity\ValidationPresence;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ValidationPresenceRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    public function init()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->entityManager->flush();
    }

    public function end()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testAddPresence()
    {
        $this->init();

        $repository = $this->entityManager
            ->getRepository(User::class);
        $student = $repository->findOneBy(["username" => "student"]);
        $student = $student->getInformationStudent();
        $this->assertNotEquals(null, $student);

        $teacher = $repository->findOneBy(["username" => "teacher"]);
        $this->assertNotEquals(null, $teacher);

        $studentGroup = $teacher->getInformationTeacher()->getStudentGroups()[0];
        $this->assertNotEquals(null, $studentGroup);

        $course = $studentGroup->getCourses()[1];
        $this->assertNotEquals(null, $course);

        $repository = $this->entityManager
            ->getRepository(ValidationPresence::class);
        $nbValidationPresenceBefore = count($repository->findAll());

        $repository->addPresence($student, $course);
        $nbValidationPresenceAfter = count($repository->findAll());

        $repository->addPresence($student, $course);
        $nbValidationPresenceAfterDuplicate = count($repository->findAll());

        
        //cas ou la ligne ajouté n'est pas deja presente
        $this->assertEquals($nbValidationPresenceBefore + 1, $nbValidationPresenceAfter);

        
        //cas ou la ligne ajouté est deja presente
        $this->assertEquals($nbValidationPresenceAfter , $nbValidationPresenceAfterDuplicate);

        $this->end();
    }
}
